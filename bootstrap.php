<?
/*
define('APP_ROOT', __DIR__);
return [
    'settings' => [
        'displayErrorDetails' => true,
        'determineRouteBeforeAppMiddleware' => false,

        'doctrine' => [
            // if true, metadata caching is forcefully disabled
            'dev_mode' => true,

            // path where the compiled metadata info will be cached
            // make sure the path exists and it is writable
            'cache_dir' =>  APP_ROOT.'/academico/doctrine/cache',

            // you should add any other path containing annotated entity classes
            'metadata_dirs' => [ APP_ROOT.'/academico/doctrine'],

            'connection' => [
                'driver' => $_ENV["DB_DRIVER"],
                'host' =>  $_ENV["DB_HOST"],
                'port' =>  $_ENV["DB_PORT"],
                'dbname' =>  $_ENV["DB_NAME"],
                'user' =>  $_ENV["DB_USER"],
                'password' =>  $_ENV["DB_PASSWORD"],
                'charset' => 'utf-8'
            ]
        ]
    ]
];
*/


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/vendor/autoload.php';
require_once '/bootstrap.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(__DIR__);
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver'   => 'pgsql',
    'user'     => 'postgres',
    'password' => '123456',
    'dbname'   => 'postgres',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

?>