<?php
namespace Conexao;

use PDO;
class Conexao
{
    private static $pdo;

    public static function getInstance() 
    {  
        $opt =    
        [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => true
        ];
        $dsn ="{$_ENV["DB_DRIVER"]}:host={$_ENV['DB_HOST']};port={$_ENV['DB_PORT']};dbname={$_ENV['DB_NAME']}";

        if (!isset(self::$pdo))
        {  
          try 
          {  
            self::$pdo =  new PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASSWORD'], $opt);
          } 
          catch (PDOException $e)
          {  
            print "Erro: " . $e->getMessage();  
          }  
        }      
        return self::$pdo;  
    } 
}



    


/*
// Get container
$container = $app->getContainer();

// Inject a new instance of PDO in the container
$container['dbh'] = function($container) {

        $config = $container->get('settings')['pdo'];
        $dsn = "{$config['engine']}:host={$config['host']};dbname={$config['database']};charset={$config['charset']}";
        $username = $config['username'];
        $password = $config['password'];

        return new PDO($dsn, $username, $password, $config['options']);

};
*/
?>