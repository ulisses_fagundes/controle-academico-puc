<?php
namespace EloquentConfig;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher as Dispatcher;
use Illuminate\Container\Container;
class EloquentConfig
{
    public static function getSettings()
    {
        return array
        (    
            'driver'    => $_ENV["DB_DRIVER"],
            'host'      => $_ENV["DB_HOST"],
            'database'  => $_ENV["DB_NAME"],
            'username'  => $_ENV["DB_USER"],
            'password'  => $_ENV["DB_PASSWORD"],
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        );
    }

   public static function getCapsule()
   {
        $settings = self::getSettings();
        $capsule = new Capsule;
        $capsule->addConnection( $settings );
        $capsule->setAsGlobal();
        $capsule->setEventDispatcher(new Dispatcher (new Container));
        $capsule->bootEloquent();
        return $capsule;
   }
}




?>