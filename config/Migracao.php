<?php
namespace Puc\Migracao;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;
use Dotenv;

require $_SERVER["DOCUMENT_ROOT"].'/academico/vendor/autoload.php';
require $_SERVER["DOCUMENT_ROOT"].'/academico/app/aluno/class/controller/aluno.php';
require $_SERVER["DOCUMENT_ROOT"].'/academico/helper/Twig.php';

$dotenv = new Dotenv\Dotenv(__DIR__.'/../');
// $dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

class Migration extends AbstractMigration {
    /** @var \Illuminate\Database\Capsule\Manager $capsule */
    public $capsule;
    /** @var \Illuminate\Database\Schema\Builder $capsule */
    public $schema;

    public function init() {
        $this->capsule = new Capsule;
        $this->capsule->addConnection([
          'driver'    => $_ENV["DB_DRIVER"],
          'host'      => $_ENV["DB_HOST"],
          'port'      => $_ENV["DB_PORT"],
          'database'  => $_ENV["DB_NAME"],
          'username'  => $_ENV["DB_USER"],
          'password'  => $_ENV["DB_PASSWORD"],
          'charset'   => 'utf8',
          'collation' => 'utf8_unicode_ci',
        ]);

        $this->capsule->bootEloquent();
        $this->capsule->setAsGlobal();
        $this->schema = $this->capsule->schema();
    }
}

?>