<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Conexao as Conexao;
use Illuminate\Database\Query\Builder;

use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/Conexao.php';

require_once $_ENV["BASE_DIR"]. "/config/EloquentConfig.php";
require_once $_ENV["BASE_DIR"]. "/helper/Menu.php";

require_once $_ENV["BASE_DIR"]."/app/departamento/class/model/Departamento.php";
require_once $_ENV["BASE_DIR"]."/app/instituto/class/model/Instituto.php";
if(is_dir($_ENV["BASE_DIR"]. "/app"))
{
  $files = scandir($_ENV["BASE_DIR"]. "/app");
  foreach($files as $file) 
  {
    if(is_dir($_ENV["BASE_DIR"]. '/app'. "/". $file. "/class/controller"))
    {
      $controllers = scandir($_ENV["BASE_DIR"]. '/app'. "/". $file. "/class/controller");
      $controllers = array_diff($controllers, array('.', '..'));
     
      if(isset($controllers) && count($controllers)> 0)
      {
        foreach($controllers as $controller)
        {
          require_once $_ENV["BASE_DIR"]. '/app'. "/". $file. "/class/controller/".$controller;
        }

      }
    }
    
  }
}


$app->get('/login', "\HomeController:main");
$app->get('/sair', "\HomeController:logout");
$app->post('/login', "\HomeController:login");

$app->group('', function() 
{

  $this->get('/', "\HomeController:main");

  $this->get('/cursos', "\CursoController:curso");
  $this->get('/cursos/{id}', "\CursoController:getDadosCurso");
  $this->post('/cursos', "\CursoController:cursoJson");  

  $this->post('/cursos_autocomplete', "\CursoController:getCursoAutoComplete");  

  $this->post('/cursos_salvar', "\CursoController:salvarCursoController");  
  $this->post('/cursos_remover', "\CursoController:removerCursoController");  

  $this->get('/disciplina', "\DisciplinaController:getDisciplinaGrid");
  $this->get('/disciplina/{id}', "\DisciplinaController:getDadosDisciplina");
  $this->post('/disciplina', "\DisciplinaController:getDisciplinaJson");  
  $this->post('/disciplina_remover', "\DisciplinaController:removerDisciplinaController");  //
  $this->post('/disciplina_salvar', "\DisciplinaController:salvarDisciplinaController");  //


  $this->post('/disciplina_autocomplete', "\DisciplinaController:getDisciplinaAutoComplete");  


  $this->post('/disciplina_grade_remover', "\GradeDisciplinaController:removerDisciplinaGradeById");
  $this->post('/disciplina_grade_salvar', "\GradeDisciplinaController:salvarGradeDisciplinaController");

  $this->post('/disciplina_grade', "\GradeDisciplinaController:getDisciplinaGradeById");

  $this->post('/grade_requisito_salvar', "\GradeDisciplinaRequisitoController:salvarRequisitoController");
  $this->post('/grade_requisito', "\GradeDisciplinaRequisitoController:getRequisitoGridController");
  $this->post('/grade_requisito_get', "\GradeDisciplinaRequisitoController:getRequisitoFormController");  
  $this->post('/grade_requisito_remover', "\GradeDisciplinaRequisitoController:removerRequisitoController");  


  $this->get('/grade', "\GradeController:getGradeGrid");
  $this->get('/grade/{id}', "\GradeController:getDadosGrade");
  $this->post('/grade', "\GradeController:getGradeJson");
  $this->post('/grade_disciplina', "\GradeController:getDisciplinaGrade");  

  $this->get('/grade_remover', "\GradeController:removerGradeController"); //
  $this->post('/grade_salvar', "\GradeController:salvarGradeController");    //

  $this->get('/inscricao', "\ProcessoSeletivoController:processoSeletivo");



  $this->get('/api/cursos', "\CursoController:cursoJson");  

  $this->post('/pessoa_autocomplete', "\PessoaController:getPessoaAutoComplete");  
});


?>


