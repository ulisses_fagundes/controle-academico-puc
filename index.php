<?php 
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Teste as Teste;
use \Conexao as Conexao;
use \EloquentConfig as EloquentConfig;
use Illuminate\Database\Eloquent;
//Initialize Illuminate Database Connection

use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

use Illuminate\Container\Capsule;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once $_SERVER["DOCUMENT_ROOT"].'/academico/vendor/autoload.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/academico/app/aluno/class/controller/aluno.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/academico/helper/Twig.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/Conexao.php';


$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

session_start();

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ],
];

$c = new \Slim\Container($configuration); 

$c['notFoundHandler'] = function ($c) 
{
    return function ($request, $response) use ($c) 
    {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('qqPage not found');
    };
};
$c["twig"] = (new TwigHelper())->getTwig();

$c["twig"]->addGlobal("dir", $_ENV["BASE_DIR"]);
// $app->view->setTemplatesDirectory(INC_ROOT . '/app/views');

$c["pdo"] = \Conexao\Conexao::getInstance();
$c["menu"] = null;
$c["usuario"] = null;

if(isset($_SESSION["usuario"]))
{
    $c["usuario"] = $_SESSION["usuario"];
    $c["menu"] = Menu::getMenu($c["pdo"] , $_SESSION["usuario"]);
}

$c["params"] = array
(
    "menu" => $c["menu"],
    "baseUrl" => $_ENV["BASE_URL"],
    "logado" =>  (isset($c["usuario"]) && $c["usuario"] != "") ? true : false   
);

$app = new \Slim\App($c);

require $_SERVER["DOCUMENT_ROOT"].'/academico/config/route.php';



$app->run();

?>

