<?php
class TwigHelper
{
    public $loader;
    public $twig;


    public function __construct ()
    {
        $this->loader = new Twig_Loader_Filesystem($_ENV["BASE_DIR"]);
        $this->twig = new Twig_Environment($this->loader);
    }
    
    public function getTwig()
    {
        return $this->twig;
    }    
}

class TwigFactory
{
    public static function getTwig()
    {
        return (new TwigHelper())->getTwig();
    } 
}

?>