<?php


class Menu
{
    private static $queryMenu = 
    "
        select 
            distinct
            t.nome tela_nome,
            m.nome menu_nome,
            m.grupo menu_grupo,
            mp.nome pai_nome,
            mp.ordem pai_ordem,
            m.ordem menu_ordem,
            t.path tela_path
        from 
            tela t
            inner join menu m on t.menu = m.menu
            inner join menu mp on mp.menu = m.menu_pai
            inner join permissao_tela pt on pt.tela = t.tela
            inner join permissao_usuario pu on pu.permissao = pt.permissao and pu.usuario = :usuario
        where
            t.tela_tipo = 3 and t.disponivel_menu = true
        order by 
            mp.ordem, mp.nome, m.grupo nulls last, m.ordem, m.nome
    ";

    private static $queryPermissaoTela =
    "
        select 
            1 
        from 
            tela t
            inner join permissao_tela pt on t.tela = pt.tela
            inner join permissao p on p.permissao = pt.permissao
            inner join permissao_usuario pu on pu.permissao = p.permissao
        where 
            t.path = :path and pu.usuario = :usuario    
    ";


    public static function getMenu($pdo, $usuario)
    {
        
        $return = null;
        $pst = $pdo->prepare(self::$queryMenu);
       
        $pst->execute(array("usuario" => $usuario));

        $result = $pst->fetchAll();
        
        return $result;
    }

    public static function getPermissaoTela($pdo, $usuario, $path)
    {
        $return = null;
        $pst = $pdo->prepare(self::$queryPermissaoTela);

        $pst->execute(array("usuario" => $usuario, "path" => $path));

        $result = $pst->fetchAll();

        return count($result) > 0 ? true : false;
    }

    public static function verificarPermissaoTela($pdo, $usuario, $request, $response,$path = null)
    {
       
        $permissaoValida = false;
        if(!isset($usuario))
        {
            $permissaoValida = false;
            echo "Usuário não está logado"; exit;
        }
        else
        {
            if($path == null) $path = $request->getUri()->getPath();
            $permissaoValida = self::getPermissaoTela($pdo, $usuario, $path);
        }

        if(!$permissaoValida)
        {
            echo "Permissão não é válida"; exit;
            return $response->withRedirect("aaaa");
        
        }
  
        return $permissaoValida;
    }
    




}
?>

