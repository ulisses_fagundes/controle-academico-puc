
function exibirAlerta(mensagem,titulo, div = null)
{
  if(div == null)
  {
    div = $(".msgAlert");
  }
  if(titulo == null) titulo = "";

  $(div).find("strong").text(mensagem);

  $(div).dialog({
    title: titulo,
    dialogClass: 'no-close success-dialog',
    modal: true,
    buttons : [{
        text : "Ok",
        click : function() 
        {
          $(this).dialog("close");                               
        }
        }],
  });
}

function resetarFormulario(form)
{
   var elementoFormulario = $(form).find("input, select, textarea");
   $(elementoFormulario).each(function(index, elem)
   {
      if($(elem).attr("data-default") )
      {
        $(elem).val( $(elem).attr("data-default") );
      }
      else
      {
        $(elem).val("");
      }
   });
}

