<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Curso extends Model
{
   protected $table = 'curso';
   protected $fillable = ["curso", "departamento", "coordenador", "nome", "curso_tipo"];
   public $timestamps  = false;
   protected $primaryKey = "curso";

   public function CursoTipo()
   {
       return $this->belongsTo("\App\Models\CursoTipo","curso_tipo");
   }

   public function Departamento()
   {
       return $this->belongsTo("\App\Models\Departamento","departamento");
   }

    public function buscarCurso($pdo, $twigParams, $queryParams = null)
    {
        
        $limit = $queryParams["paginacao"]["limit"] ?? 10;
        $pagina = !isset($queryParams["pagina"]) ? 1 : $queryParams["pagina"];

        $offset = ($pagina - 1)  * $limit;

        $parametros = array("curso_nome" => "", "limit" => $limit, "offset" => $offset);

        $orderByPermitidos = array
        (
            "curso" => "curso", 
            "curso_nome" => "curso_nome", 
            "departamento_nome" => "departamento_nome", 
            "instituto_nome" => "instituto_nome", 
            "tipo" => "tipo", 
            "grau" => "grau", 
            "coordenador_nome" => "coordenador_nome"
        );

        $query = 
        "
          select 
            c.curso,
            c.nome curso_nome,
            d.nome departamento_nome,
            i.nome instituto_nome,
            ct.nome tipo,
            cg.nome grau,
            coo.nome coordenador_nome,
            count(*) over()	total
          from 
            curso c
            inner join departamento d on c.departamento = d.departamento
            inner join instituto i on d.instituto = i.instituto
            inner join pessoa coo on c.coordenador = coo.pessoa
            inner join curso_tipo ct on ct.curso_tipo = c.curso_tipo
            inner join curso_grau cg on cg.curso_grau = ct.curso_grau 
          where
            c.nome ilike '%' || :curso_nome || '%' 
          
        ";
        if(is_array($queryParams))
        {
            if(array_key_exists("orderBy", $queryParams))
            {
                if(isset($queryParams["orderBy"]["order"]) && in_array($queryParams["orderBy"]["order"], $orderByPermitidos))
                {
                    $asc = (isset($queryParams["orderBy"]["asc"]) && $queryParams["orderBy"]["asc"] == "asc") ? "asc" : "desc";
                    $query .= " order by ". $orderByPermitidos[$queryParams["orderBy"]["order"]] . " " . $asc;
 
                }
            }    
            if(isset ($queryParams["palavraChave"]) && $queryParams["palavraChave"]!= "") 
            {
                $parametros["curso_nome"] = $queryParams["palavraChave"];
            }   
        }
        $query .= " limit :limit offset :offset ";

        $pst = $pdo->prepare($query);
        
        $pst->execute($parametros);
    
        $result = $pst->fetchAll();

        $twigParams["paginacao"] = array();
        
        @$twigParams["paginacao"]["quantidadePagina"] = ceil($result[0]["total"] / $limit);
        $twigParams["paginacao"]["atual"] = ceil($offset  / $limit) + 1;
        @$twigParams["paginacao"]["fim"] = min(($offset + $limit), $result[0]["total"]);
        @$twigParams["paginacao"]["total"] = $result[0]["total"];
        $twigParams["paginacao"]["limite"] = $limit;

        $twigParams["cursos"] = $result;
        $twigParams["cursosOpcoes"] = 
        '
         <a class="edit opcaoGrid" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons">&#xE254;</i></a>
         ';

        return $twigParams;
    }

    public function getDadosCurso($id)
    {
        $id = array("id" => $id);

        return $this
            ->with("Departamento", "departamento") 
            ->with("CursoTipo","CursoTipo")
            ->join('pessoa', 'pessoa.pessoa', '=', 'curso.coordenador')
            ->select("curso", "curso.ativo as ativo", "curso.departamento", "curso.curso_tipo", "curso.coordenador", "curso.nome", "pessoa.pessoa","pessoa.nome as pessoa_nome")
            ->find($id)
            
        ;
    }


    public function salvarCurso($curso)
    {
         if(isset($curso["curso"]) && $curso["curso"] != "" )
         {
             $sucesso = $this       
                 ->where('curso', $curso["curso"])
                 ->update
                 (
                     array
                     (
                        'departamento' => $curso["departamento"],
                        'coordenador' => $curso["coordenador"],
                        'nome' => $curso["curso_nome"],
                        'curso_tipo' => $curso["curso_tipo"],
                        "ativo" => (isset($curso["ativo"]) && $curso["ativo"]== true) ? true : false
                     )
                 )
             ;
         }    
         else
         {
             $sucesso = $this       
                 ->insert
                 (
                     array
                     (
                        'departamento' => $curso["departamento"],
                        'coordenador' => $curso["coordenador"],
                        'nome' => $curso["curso_nome"],
                        'curso_tipo' => $curso["curso_tipo"],
                        "ativo" => (isset($curso["ativo"]) && $curso["ativo"]== true) ? true : false
                     )
                 )
             ;
         }
 
         return array("sucesso" => $sucesso);
    } 
    
    public function removerCurso($id)
    {        
         $sucesso = $this       
             ->where('curso', $id)
             ->update(array('ativo' => "false"))
         ;  
         return array("sucesso" => $sucesso);
    }
    
    public function getCursoAutoComplete($params)
    {
   
        return array
        (
            'list' =>
             $this
                 ->select("curso", "nome as label")
                 ->where('nome', 'ilike','%'.  ($params["termo"]) . '%')
                 ->limit($params["limite"])
                 ->get()
             
         );
 
    }    
    

}

?>