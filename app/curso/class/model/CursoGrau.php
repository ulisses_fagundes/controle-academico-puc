<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class CursoGrau extends Model
{
   protected $table = 'curso_grau';
   protected $fillable = ["curso_grau", "nome"];
   public $timestamps  = false;
   protected $primaryKey = "curso_grau";

}

?>