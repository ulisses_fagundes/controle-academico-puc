<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
require_once $_ENV["BASE_DIR"]."/app/curso/class/model/CursoGrau.php";

class CursoTipo extends Model
{
   protected $table = 'curso_tipo';
   protected $fillable = ["curso_tipo", "nome", "curso_grau"];
   public $timestamps  = false;
   protected $primaryKey = "curso_tipo";

   public function CursoGrau()
   {
       return $this->belongsTo("\App\Models\CursoGrau","curso_grau");
   }

   public function getCursoTipo()
   {
    return $this::with("CursoGrau")->get();
   }

}

?>