<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/curso/class/model/Curso.php";
require_once $_ENV["BASE_DIR"]."/app/curso/class/model/CursoTipo.php";
require_once $_ENV["BASE_DIR"]."/app/departamento/class/model/Departamento.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class CursoController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();
     //  $this->usuario = new \App\Models\Usuario();
      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->curso = new \App\Models\Curso();
      
  }

  public  function curso(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $params["dados"] = array();
    $pdo = $this->pdo;
    $curso = $this->curso;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];    
    $return = array();
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response);
    $result = $curso->buscarCurso($pdo, $params);

    if(count($result)> 0)
    {
      $params["dados"]["curso"] = $result;


      $params["dados"]["departamento"] = (new \App\Models\Departamento())->getDepartamentos();
      $params["dados"]["cursoTipo"] = (new \App\Models\CursoTipo())->getCursoTipo();

      return $response->getBody()->write($twig->render('/app/curso/template/curso.html.twig',$params));
    }
    else
    {
      // errorPage
    }

     return $response->getBody()->write($twig->render('/app/curso/template/curso.html.twig',$params));

  } 

  public  function cursoJson(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $curso = $this->curso;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array();    


    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "cursos");
    $result = $curso->buscarCurso($pdo, $params, $filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }
  }  

  public function getDadosCurso(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $curso = $this->curso;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array();
   
    $id = $args["id"];

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "cursos");
    $result = $curso->getDadosCurso($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }



  public function salvarCursoController(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $curso = $this->curso;
    $parsedBody = $request->getParsedBody();
    $dados = $parsedBody["curso"];
    $return = array();

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response,"cursos");

    $result = $curso->salvarCurso($dados);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }  

  public function removerCursoController(Request $request, Response $response, array $args)
  { 
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $curso = $this->curso;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();
   
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "curso");

    $result = $curso->removerCurso($id);

    if(count($result)> 0)
    {
      return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }    


  public  function getCursoAutoComplete(Request $request, Response $response, array $args)
  {

    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $curso = $this->curso;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array(); 
    $permissoesPermitidas = array
    (
      "cursos" => "cursos"
    );   

    $permissao = null;
    if(isset($parsedBody["origem"]) && in_array($parsedBody["origem"], $permissoesPermitidas))
    {
      $permissao = $permissoesPermitidas[$parsedBody["origem"]];
    }

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, $permissao);
    $result = $curso->getCursoAutoComplete($filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }
  }




}



?>