$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    preencherCursos();
    preencherPaginacao();
    $(".sortOption").bind("click", orderSort);
    $("#buscaPalavraChave").bind("keyup", buscarCursos);
    preencherCursoTipo();
    preencherDepartamento();

    $("#c-departamento").bind("change", changeSelectDepartamento);
    $("#c-tipo").bind("change", changeSelectCursoTipo);

    $("#cancelarCurso").bind("click", fecharFormularioCurso);
    $("#salvarCurso").bind("click",salvarCurso);

    $("#btnNovoCurso").bind("click", novoCurso);

    autoCompleteCoordenador($("#c-coordenador"));

});

function changeSelectCursoTipo() {
    var select = $(this);

    if (select != "") {
        $("#c-grau").val(select.find(":selected").attr("data-grau"));
    } else {
        $("#c-grau").val("");
    }
}

function changeSelectDepartamento() {
    var select = $(this);

    if (select != "") {
        $("#c-instituto").val(select.find(":selected").attr("data-instituto"));
    } else {
        $("#c-instituto").val("");
    }
}

function preencherCursoTipo() {
    var dados = $("#hdnCursosTipo").data("dados");
    $(dados).each(function (index, elem) {
        $("#c-tipo").append($("<option>", {
            value: elem.curso_tipo,
            text: elem.nome

        }).attr("data-grau", elem.curso_grau.nome))
    });
}
function preencherDepartamento() {
    var dados = $("#hdnDepartamentos").data("dados");
    $(dados).each(function (index, elem) {
        $("#c-departamento").append($("<option>", {
            value: elem.departamento,
            text: elem.nome
            //data-instituto: elem.instituto.nome
        }).attr("data-instituto", elem.instituto.nome))
    });

}

function orderSort() {
    var filtro = $(this);

    $(".sortOption")
        .find(".fa")
        .not(function () {

            return $(filtro).data("sort") == $(this).closest(".sortOption").data("sort") ? true : false;
        })
        .addClass("fa-sort")
        .removeClass("fa-sort-down")
        .removeClass("fa-sort-up")
        .removeClass("selected");

    if (filtro.find(".fa").hasClass("fa-sort-down") == true) {

        filtro
            .find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-down")
            .addClass("fa-sort-up")
            .addClass("selected");
    } else {

        filtro
            .find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-up")
            .addClass("fa-sort-down")
            .addClass("selected");
    }

    buscarCursos();
    preencherPaginacao();

}

function buscarCursos(event = null) {

    var filtro = {};
    filtro.orderBy = {};
    filtro.orderBy.order = $(".sortOption").find("i").filter(".selected").closest(".sortOption").attr("data-sort");
    filtro.orderBy.asc = $(".sortOption").find("i").filter(".selected").filter(".fa-sort-up").length > 0 ? "asc" : "desc";
    filtro.palavraChave = $("#buscaPalavraChave").val();
    filtro.pagina = 1;

    if (event != null && event.data != undefined && event.data != null) {
        filtro.pagina = event.data.pagina;
    }

    $.ajax({
        url: "cursos",
        method: "POST",
        dataType: "html",
        data: {
            filtro: filtro
        },
        success: function (data) {
            var result = JSON.parse(data);

            $("#hdnCursos").data("dados", result.cursos);
            $("#hdnPaginacao").data("dados", result.paginacao);
            preencherCursos();
            preencherPaginacao();

        }
    });

}

function preencherPaginacao() {

    var paginacao = $("#hdnPaginacao").data("dados");
    var desabilitarPrimeiraPagina = (paginacao.atual == null || paginacao.atual == 1) ? "disabled" : "";
    var desabilitarUltimaPagina = (paginacao.atual == null || paginacao.atual == paginacao.quantidadePagina) ? "disabled" : "";

    $(".pagination").empty();
    $(".pagination").append($("<li class='page-item " + desabilitarPrimeiraPagina + "'>").append('<a href="#"><i class="fa fa-angle-double-left"></i></a>').bind("click", {
        pagina: 1
    }, buscarCursos));

    var i;
    var limite = paginacao.quantidadePagina == null ? 1 : paginacao.quantidadePagina;
    for (i = 1; i <= limite; i++) {
        $(".pagination").append($("<li class='page-item lipg-" + i + "'>").append('<a href="#" class="page-link">' + i + '</a>').bind("click", {
            pagina: i
        }, buscarCursos));
    }

    $(".pagination").append($("<li class='page-item " + desabilitarUltimaPagina + "'>").append('<a href="#"><i class="fa fa-angle-double-right"></i></a>').bind("click", {
        pagina: paginacao.quantidadePagina
    }, buscarCursos));

    $(".lipg-" + paginacao.atual).addClass("active");

}

function preencherCursos(event = null) {
    var cursos = $("#hdnCursos").data("dados");
    $("#tbodyCursos").empty();
    $(cursos).each(function (index, elem) {
        $("#tbodyCursos").append($("<tr id=trId_'" + elem.curso + "' data-id='" + elem.curso + "'> ").append($("<td colspan='1'>").append(elem.curso)).append($("<td colspan='1'>").append(elem.curso_nome)).append($("<td colspan='1'>").append(elem.departamento_nome)).append($("<td colspan='1'>").append(elem.instituto_nome)).append($("<td colspan='1'>").append(elem.grau)).append($("<td colspan='1'>").append(elem.coordenador_nome)).append($("<td colspan='1'>").html(JSON.parse($("#hdnCursosOpcoes").data("dados")))));

    });

    $(".opcaoGrid").filter(".edit").on("click", editCurso);
    $(".opcaoGrid").filter(".view").on("click", function () {
        console.log("view");
    })
    $(".opcaoGrid").filter(".delete").on("click", function () {
        console.log("delete");
    })
}

function editCurso() {
    var botao = $(this);
    var id = $(botao).closest("tr").attr("data-id");

    $.ajax({
        url: "cursos/" + id,
        method: "GET",
        dataType: "html",
        data: {
            id: id
        },
        success: function (data) {
            var result = JSON.parse(data);
            resetarFormulario(  $(".formCurso") );
            $(".formCurso").show();

            $("#hdnFormCurso").val(result[0].curso);
            $("#c-nome").val(result[0].nome);
            $("#c-departamento").val(result[0].departamento.departamento).change();
            $("#c-coordenadorId").val(result[0].pessoa);
            $("#c-coordenador").val(result[0].pessoa_nome);
            $("#c-tipo").val(result[0].curso_tipo.curso_tipo).change();
            $("#c-ativo").val(result[0].ativo == true ? "true" : "false");

        }
    });
}

function salvarCurso() 
{

    var curso = {};

    curso.curso = $("#hdnFormCurso").val();
    curso.departamento = $("#c-departamento").val();
    curso.coordenador = $("#c-coordenadorId").val();
    curso.curso_nome = $("#c-nome").val();
    curso.curso_tipo = $("#c-tipo").val();
    curso.ativo = $("#c-ativo").val();

    $.ajax({
        url: "/academico/cursos_salvar",
        method: "post",
        dataType: "html",
        data: {
            curso: curso
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.sucesso)
            {
                fecharFormularioCurso();      
                exibirAlerta("Curso salvo com sucesso.","Sucesso", $(".msgAlert"));
               
                orderSort();
            }
            else
            {
                exibirAlerta(result.msg ? result.msg : "Ocorreu uma falha ao salvar o curso","Falha ao salvar", $(".msgAlert"));
            }
        }
    });

}


function fecharFormularioCurso()
{
    $(".formCurso").hide();
}

function novoCurso()
{
    resetarFormulario(  $(".formCurso") );
    $(".formCurso").show();
}

function autoCompleteCoordenador(jQueryObject)
{
    jQueryObject.autocomplete
    ({
        source: function (request, response) 
        {
            $.ajax({
                type: "post",
                url: "pessoa_autocomplete",
                dataType: "html",
                async: false,
                cache: false,
                data: 
                {
                    filtro: 
                    {
                        termo: request.term,
                        limite: 20,
                    },
                    origem: "cursos"

                },
                success: function (data)
                {
                    var result = $.parseJSON(data);
                    response(result.list);
                }
            });
        },
        minLength: 0,
        select: function (event, ui) 
        {
            $(this).val(ui.item.label);
            $(this).closest(".autoComplete").find(".autoCompleteId").val(ui.item.pessoa);
            return false;
        },
        change: function(event, ui) 
        {
            if(ui.item == null || ui.item === undefined)
            {
                $(this).val("");
                $(this).closest(".autoComplete").find(".autoCompleteId").val("");
            }
            return false;
        }
    }, 
    {
        selectFirst: true,
        minLength: 0
    });
}

