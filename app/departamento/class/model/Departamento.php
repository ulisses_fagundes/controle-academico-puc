<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
require_once $_ENV["BASE_DIR"]."/app/instituto/class/model/Instituto.php";

class Departamento extends Model
{
   protected $table = 'departamento';
   protected $fillable = ["departamento",  "nome", "instituto"];
   public $timestamps  = false;
   protected $primaryKey = "departamento";

   public function Instituto()
   {
       return $this->belongsTo("\App\Models\Instituto","instituto");
   }

   public function getDepartamentos()
   {
       return $this::with("Instituto")->get();
   }

}

?>