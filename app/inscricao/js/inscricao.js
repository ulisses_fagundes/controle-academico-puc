$(document).ready(function()
{
    console.log("xxxx");
    preencherProcesoSeletivo();
    preencherPaginacao();
   
    $(".sortOption").bind("click", orderSort);
    //$("#c-departamento").bind("change", changeSelectDepartamento);
});

function orderSort()
{
    var filtro = $(this);

    $(".sortOption").find(".fa").not
        (
            function()
            {
                return $(filtro).data("sort") == $(this).closest(".sortOption").data("sort") ? true : false;
            }
        )
        .addClass("fa-sort")
        .removeClass("fa-sort-down")
        .removeClass("fa-sort-up")
        .removeClass("selected")
    ;

    if(filtro.find(".fa").hasClass("fa-sort-down") == true)
    {      
        filtro.find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-down")
            .addClass("fa-sort-up")
            .addClass("selected")
        ;
    }
    else
    {   
        filtro.find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-up")
            .addClass("fa-sort-down")
            .addClass("selected")
        ;
    }
    
    preencherProcesoSeletivo();
    preencherPaginacao();    
}

function buscarDisciplina(event = null)
{

    var filtro = {};
    filtro.orderBy = {};
    filtro.orderBy.order = $(".sortOption").find("i").filter(".selected").closest(".sortOption").attr("data-sort");
    filtro.orderBy.asc =  $(".sortOption").find("i").filter(".selected").filter(".fa-sort-up").length > 0 ? "asc" : "desc";
    filtro.palavraChave = $("#buscaPalavraChave").val();
    filtro.pagina = 1;

    if(event !=  null)
    if(event.data != null && event.data != undefined)
    {
        filtro.pagina = event.data.pagina;
    }

    $.ajax
    ({
        url: "disciplina",
        method: "POST",
        dataType: "html",
        data: 
        {
            filtro: filtro
        },
        success: function(data)
        {
            var result = JSON.parse(data);

            $("#hdnDisciplinas").data("dados", result.result);
            $("#hdnPaginacao").data("dados", result.paginacao);
            preencherProcessoSeletivo();
            preencherPaginacao();
        }
    });


}

function preencherProcessoSeletivo()
{

    var paginacao = $("#hdnPaginacao").data("dados");
    var desabilitarPrimeiraPagina = (paginacao.atual == null || paginacao.atual == 1) ? "disabled" : ""; 
    var desabilitarUltimaPagina = (paginacao.atual == null || paginacao.atual == paginacao.quantidadePagina) ? "disabled" : ""; 
    
    $(".pagination").empty();
    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarPrimeiraPagina + "'>").append
        (
            '<a href="#"><i class="fa fa-angle-double-left"></i></a>'
        ).bind("click", {pagina: 1}, buscarDisciplina)
    );

    var i;
    var limite = paginacao.quantidadePagina == null ? 1 : paginacao.quantidadePagina;
    for(i = 1;i<= limite; i++)
    {
        $(".pagination").append
        (
            $("<li class='page-item lipg-"  + i + "'>").append
            (
                '<a href="#" class="page-link">'+ i +'</a>'
            ).bind("click", {pagina: i}, buscarDisciplina)           
        );
    }

    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarUltimaPagina + "'>").append
        (
            '<a href="#"><i class="fa fa-angle-double-right"></i></a>'
        ).bind("click", {pagina: paginacao.quantidadePagina}, buscarDisciplina)
    );    

    $(".lipg-"+ paginacao.atual).addClass("active");

}

function preencherProcesoSeletivo(event = null)
{
    var conteudoInscrever = $("#conteudoInscrever").clone();

    var processoSeletivo = $("#hdnProcessosSeletivos").data("dados");
    $("#tbodyProcessoSeletivo").empty();

    console.log("aqui");
    console.log(processoSeletivo);;
    $(processoSeletivo).each(function(index, elem)
    {
        console.log("aa");
        $("#tbodyProcessoSeletivo").append
        (
            $("<tr id=trId_'" + elem.processo_seletivo + "' data-id='"+elem.processo_seletivo+"'> ")
            .append
            (
                $("<td colspan='1'>").append(elem.processo_seletivo)
            )	            
                .append
                (
                    $("<td colspan='1'>").append(elem.ano_periodo)
                )	
                .append
                (
                    $("<td colspan='1'>").append(elem.curso_nome)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.inscricao_inicio + elem.inscricao_fim)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.data_exame)
                )
                .append
                (
                    $("<td colspan='1'>").append
                    (
                        $("<button class='btn btn-default btn-xs'>").append
                        (
                            $("<a>").append
                            (
                                elem.edital_nome
                            )
                        )
                  
                    )
                )
                .append
                (
                    $("<td colspan='1'>").append
                    (
                        $(conteudoInscrever).html()
                    )
                )

        );    

    });
}

function preencherPaginacao()
{

    var paginacao = $("#hdnPaginacao").data("dados");
    var desabilitarPrimeiraPagina = (paginacao.atual == null || paginacao.atual == 1) ? "disabled" : ""; 
    var desabilitarUltimaPagina = (paginacao.atual == null || paginacao.atual == paginacao.quantidadePagina) ? "disabled" : ""; 
    
    $(".pagination").empty();
    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarPrimeiraPagina + "'>").append
        (
            '<a href="#"><i class="fa fa-angle-double-left"></i></a>'
        ).bind("click", {pagina: 1}, buscarDisciplina)
    );

    var i;
    var limite = paginacao.quantidadePagina == null ? 1 : paginacao.quantidadePagina;
    for(i = 1;i<= limite; i++)
    {
        $(".pagination").append
        (
            $("<li class='page-item lipg-"  + i + "'>").append
            (
                '<a href="#" class="page-link">'+ i +'</a>'
            ).bind("click", {pagina: i}, buscarDisciplina)           
        );
    }

    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarUltimaPagina + "'>").append
        (
            '<a href="#"><i class="fa fa-angle-double-right"></i></a>'
        ).bind("click", {pagina: paginacao.quantidadePagina}, buscarDisciplina)
    );    

    $(".lipg-"+ paginacao.atual).addClass("active");

}   
