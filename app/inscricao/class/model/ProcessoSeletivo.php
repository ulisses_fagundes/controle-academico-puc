<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class ProcessoSeletivo extends Model
{

   protected $table = 'processo_seletivo';
   protected $fillable = ["processo_seletivo", "curso", "nome", "inscricao_inicio", "inscricao_fim", "data_exame", "edital", "status"];
   public $timestamps  = false;
   protected $primaryKey = "processo_seletivo";

    public function Curso()
    {
        return $this->belongsTo("\App\Models\Curso","curso");
    }

    public function buscarProcessoSeletivo($pdo, $twigParams, $queryParams = null)
    {
        
        $limit = $queryParams["paginacao"]["limit"] ?? 10;
        $pagina = !isset($queryParams["pagina"]) ? 1 : $queryParams["pagina"];

        $offset = ($pagina - 1)  * $limit;

        $parametros = array("processo_nome" => "", "limit" => $limit, "offset" => $offset);

        $orderByPermitidos = array
        (
            "processo_seletivo" => "processo_seletivo", 
            "processo_nome" => "processo_nome",
            "curso_nome" => "curso_nome", 
            "inscricao_inicio" => "inscricao_inicio", 
            "inscricao_fim" => "inscricao_fim", 
            "data_exame" => "data_exame", 
            "edital" => "edital", 
            "status" => "status",
            "ano_periodo" => "ano_periodo"
        );

        $query = 
        "
        select 
            ps.processo_seletivo, 
            ps.curso, 
            ps.nome processo_nome, 
            ps.inscricao_inicio, 
            ps.inscricao_fim, 
            ps.data_exame, 
            ps.edital, 
            ps.status,
            c.nome curso_nome,
            ps.ano_periodo,
            ps.edital_nome,
            count(*) over()	total
        from 
            processo_seletivo ps
            inner join curso c on ps.curso = c.curso
        where
            ps.nome ilike '%' || :processo_nome || '%'             
          
        ";
        if(is_array($queryParams))
        {
            if(array_key_exists("orderBy", $queryParams))
            {
                if(isset($queryParams["orderBy"]["order"]) && in_array($queryParams["orderBy"]["order"], $orderByPermitidos))
                {
                    $asc = (isset($queryParams["orderBy"]["asc"]) && $queryParams["orderBy"]["asc"] == "asc") ? "asc" : "desc";
                    $query .= " order by ". $orderByPermitidos[$queryParams["orderBy"]["order"]] . " " . $asc;
 
                }
            }    
            if(isset ($queryParams["palavraChave"]) && $queryParams["palavraChave"]!= "") 
            {
                $parametros["processo_nome"] = $queryParams["processo_nome"];
            }   
        }
        $query .= " limit :limit offset :offset ";

        $pst = $pdo->prepare($query);

        $pst->execute($parametros);
    
        $result = $pst->fetchAll();

        $twigParams["paginacao"] = array();
        
        @$twigParams["paginacao"]["quantidadePagina"] = ceil($result[0]["total"] / $limit);
        $twigParams["paginacao"]["atual"] = ceil($offset  / $limit) + 1;
        @$twigParams["paginacao"]["fim"] = min(($offset + $limit), $result[0]["total"]);
        @$twigParams["paginacao"]["total"] = $result[0]["total"];
        $twigParams["paginacao"]["limite"] = $limit;
     
        $twigParams["processoSeletivo"] = $result;
        /*
        $twigParams["cursosOpcoes"] = 
        '
         <a class="edit opcaoGrid" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons">&#xE254;</i></a>
         ';
        */
        return $twigParams;
    }
  

}

?>