<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/inscricao/class/model/ProcessoSeletivo.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class ProcessoSeletivoController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();
     //  $this->usuario = new \App\Models\Usuario();
      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->processoSeletivo = new \App\Models\ProcessoSeletivo();
      
  }

  public  function processoSeletivo(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $params["dados"] = array();
    $pdo = $this->pdo;
    $processoSeletivo = $this->processoSeletivo;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];    
    $return = array();
    //Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response);
    $result = $processoSeletivo->buscarProcessoSeletivo($pdo, $params);

    if(count($result)> 0)
    {
      $params["dados"]["processoSeletivo"] = $result;
      return $response->getBody()->write($twig->render('/app/inscricao/template/inscricao.html.twig',$params));
    }
    else
    {
      // errorPage
    }

     return $response->getBody()->write($twig->render('/app/inscricao/template/inscricao.html.twig',$params));

  } 

}



?>