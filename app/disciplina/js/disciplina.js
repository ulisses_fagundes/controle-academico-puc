$(document).ready(function()
{
    preencherDisciplina();
    preencherPaginacao();
    preencherDepartamento();
    $(".sortOption").bind("click", orderSort);
    $("#buscaPalavraChave").bind("keyup",buscarDisciplina);    
    $("#btnSalvarDisciplina").bind("click", salvarDisciplina);
   

    $("#cancelarDisciplina").bind("click", fecharFormularioDisciplina);

    $("#btnNovaDisciplina").bind("click", novaDisciplina);
});

function orderSort()
{
    var filtro = $(this);

    $(".sortOption").find(".fa").not
        (
            function()
            {
                return $(filtro).data("sort") == $(this).closest(".sortOption").data("sort") ? true : false;
            }
        )
        .addClass("fa-sort")
        .removeClass("fa-sort-down")
        .removeClass("fa-sort-up")
        .removeClass("selected")
    ;

    if(filtro.find(".fa").hasClass("fa-sort-down") == true)
    {      
        filtro.find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-down")
            .addClass("fa-sort-up")
            .addClass("selected")
        ;
    }
    else
    {   
        filtro.find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-up")
            .addClass("fa-sort-down")
            .addClass("selected")
        ;
    }
    
    buscarDisciplina();
    preencherPaginacao();    
}

function buscarDisciplina(event = null)
{

    var filtro = {};
    filtro.orderBy = {};
    filtro.orderBy.order = $(".sortOption").find("i").filter(".selected").closest(".sortOption").attr("data-sort");
    filtro.orderBy.asc =  $(".sortOption").find("i").filter(".selected").filter(".fa-sort-up").length > 0 ? "asc" : "desc";
    filtro.palavraChave = $("#buscaPalavraChave").val();
    filtro.pagina = 1;

    if(event !=  null)
    if(event.data != null && event.data != undefined)
    {
        filtro.pagina = event.data.pagina;
    }

    $.ajax
    ({
        url: "disciplina",
        method: "POST",
        dataType: "html",
        data: 
        {
            filtro: filtro
        },
        success: function(data)
        {
            var result = JSON.parse(data);

            $("#hdnDisciplinas").data("dados", result.result);
            $("#hdnPaginacao").data("dados", result.paginacao);
            preencherDisciplina();
            preencherPaginacao();
        }
    });


}

function preencherPaginacao()
{

    var paginacao = $("#hdnPaginacao").data("dados");
    var desabilitarPrimeiraPagina = (paginacao.atual == null || paginacao.atual == 1) ? "disabled" : ""; 
    var desabilitarUltimaPagina = (paginacao.atual == null || paginacao.atual == paginacao.quantidadePagina) ? "disabled" : ""; 
    
    $(".pagination").empty();
    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarPrimeiraPagina + "'>").append
        (
            '<a href="#"><i class="fa fa-angle-double-left"></i></a>'
        ).bind("click", {pagina: 1}, buscarDisciplina)
    );

    var i;
    var limite = paginacao.quantidadePagina == null ? 1 : paginacao.quantidadePagina;
    for(i = 1;i<= limite; i++)
    {
        $(".pagination").append
        (
            $("<li class='page-item lipg-"  + i + "'>").append
            (
                '<a href="#" class="page-link">'+ i +'</a>'
            ).bind("click", {pagina: i}, buscarDisciplina)           
        );
    }

    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarUltimaPagina + "'>").append
        (
            '<a href="#"><i class="fa fa-angle-double-right"></i></a>'
        ).bind("click", {pagina: paginacao.quantidadePagina}, buscarDisciplina)
    );    

    $(".lipg-"+ paginacao.atual).addClass("active");

}

function preencherDisciplina(event = null)
{    
    var disciplinas = $("#hdnDisciplinas").data("dados");
    $("#tbodyDisciplinas").empty();
    $(disciplinas).each(function(index, elem)
    {
        
        $("#tbodyDisciplinas").append
        (
            $("<tr id=trId_'" + elem.disciplina + "' data-id='"+elem.disciplina+"'> ")
                .append
                (
                    $("<td colspan='1'>").append(elem.disciplina)
                )	
                .append
                (
                    $("<td colspan='1'>").append(elem.disciplina_nome)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.departamento_nome)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.carga_horaria_total)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.carga_horaria_teorica)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.carga_horaria_pratica)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.carga_horaria_distancia)
                )                                                
                .append
                (
                    $("<td colspan='1'>").append(elem.ativo)
                )

                .append
                (
                    $("<td colspan='1'>").html(JSON.parse($("#hdnGridOpcoes").data("dados")))
                        
                )
        );
        
    
    });

    $(".opcaoGrid").filter(".edit").on("click", editDisciplina);
   
}
          
function editDisciplina()
{
    var botao = $(this);
    var id = $(botao).closest("tr").attr("data-id");    
    resetarFormulario(  $(".formDisciplina") );
    $.ajax
    ({
        url: "disciplina/" + id,
        method: "GET",
        dataType: "html",
        data: 
        {
            id: id
        },
        success: function(data)
        {
            var result = JSON.parse(data);
            $(".formDisciplina").show();
            resetarFormulario(  $(".formDisciplina") );
             
            $("#hdnFormDisciplina").val(result[0].disciplina);
            $("#d-nome").val(result[0].nome);
            $("#d-departamento").val(result[0].departamento ? result[0].departamento : "").change();
            $("#d-cargaHorariaTotal").val(result[0].carga_horaria_total);
            $("#d-cargaHorariaTeorica").val(result[0].carga_horaria_teorica);
            $("#d-cargaHorariaPratica").val(result[0].carga_horaria_pratica);
            $("#d-cargaHorariaDistancia").val(result[0].carga_horaria_disancia);
            $("#d-ativo").val(result[0].ativo == true ? "true" : "false");

        }
    });
}       

function salvarDisciplina()
{
 
    var disciplina = {};

    disciplina.disciplina = $("#hdnFormDisciplina").val();
    disciplina.nome = $("#d-nome").val();
    disciplina.departamento = $("#d-departamento").val();
    disciplina.carga_horaria_total = $("#d-cargaHorariaTotal").val();
    disciplina.carga_horaria_teorica = $("#d-cargaHorariaTeorica").val();
    disciplina.carga_horaria_pratica = $("#d-cargaHorariaPratica").val();
    disciplina.carga_horaria_distancia = $("#d-cargaHorariaDistancia").val();
    disciplina.ativo = $("#d-ativo").val();

    $.ajax
    ({
        url: "disciplina_salvar",
        method: "post",
        dataType: "html",
        data: 
        {
            disciplina: disciplina
        },
        success: function(data)
        {
            var result = JSON.parse(data);
            if(result.sucesso)
            {
                fecharFormularioDisciplina();      
                exibirAlerta("Disciplina salva com sucesso.","Sucesso", $(".msgAlert"));
                
                orderSort();
            }
            else
            {
                exibirAlerta(result.msg ? result.msg : "Ocorreu uma falha ao salvar a disciplina","Falha ao salvar", $(".msgAlert"));
            }            
        }
    });     

}

function removerDisciplina()
{
    var id = $(this).closest("tr").data("id");
    
    $.ajax
    ({
        url: "disciplina_remover",
        method: "post",
        dataType: "html",
        data: 
        {
            id: id
        },
        success: function(data)
        {
            var result = JSON.parse(data);        
        }
    });  
}

function preencherDepartamento() {
    var dados = $("#hdnDepartamentos").data("dados");
    $(dados).each(function (index, elem) {
        $("#d-departamento").append($("<option>", {
            value: elem.departamento,
            text: elem.nome
            //data-instituto: elem.instituto.nome
        }))
    });

}

function fecharFormularioDisciplina()
{
    $(".formDisciplina").hide();
}

function novaDisciplina()
{
    resetarFormulario(  $(".formDisciplina") );
    $(".formDisciplina").show();
}
