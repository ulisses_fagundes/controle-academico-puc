<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
   protected $table = 'disciplina';
   protected $fillable = 
   [    
       "disciplina", "nome", "departamento", "carga_horaria_total", "carga_horaria_teorica",
        "carga_horia_pratica", "carga_horaria_distancia", "ativo", "descricao"
   ];
   public $timestamps  = false;
   protected $primaryKey = "disciplina";

   public function Departamento()
   {
       return $this->belongsTo("\App\Models\Departamento","departamento");
   }

   public function buscarDisciplina($pdo, $twigParams, $queryParams = null)
   {
       
       $limit = $queryParams["paginacao"]["limit"] ?? 5;
       $pagina = !isset($queryParams["pagina"]) ? 1 : $queryParams["pagina"];

       $offset = ($pagina - 1)  * $limit;

       $parametros = array("disciplina_nome" => "", "limit" => $limit, "offset" => $offset);

       $orderByPermitidos = array
       (
           "disciplina" => "disciplina", 
           "disciplina_nome" => "disciplina_nome", 
           "departamento_nome" => "departamento_nome", 
           "departamento" => "departamento", 
           "ativo" => "ativo", 
           "carga_horaria_total" => "carga_horaria_total",
           "carga_horaria_teorica" => "carga_horaria_teorica",
           "carga_horaria_pratica" => "carga_horaria_pratica",
           "carga_horaria_distancia" => "carga_horaria_distancia"
           
       );

       $query = 
       "
        select
            di.disciplina,
            di.nome disciplina_nome,
            di.departamento,
            coalesce(di.carga_horaria_total, 0) carga_horaria_total,
            coalesce(di.carga_horaria_teorica, 0) carga_horaria_teorica,
            coalesce(di.carga_horaria_pratica, 0) carga_horaria_pratica,
            coalesce(di.carga_horaria_distancia, 0) carga_horaria_distancia,
            case di.ativo when true then 'Ativo' else 'Desativado' end ativo,
            coalesce(de.nome,'Sem departamento') departamento_nome,
            count(*) over () total      
        from 
            disciplina di
            left join departamento de on di.departamento = de.departamento
         where
           di.nome ilike '%' || :disciplina_nome || '%' 
         
       ";
       if(is_array($queryParams))
       {
           if(array_key_exists("orderBy", $queryParams))
           {
               if(isset($queryParams["orderBy"]["order"]) && in_array($queryParams["orderBy"]["order"], $orderByPermitidos))
               {
                   $asc = (isset($queryParams["orderBy"]["asc"]) && $queryParams["orderBy"]["asc"] == "asc") ? "asc" : "desc";
                   $query .= " order by ". $orderByPermitidos[$queryParams["orderBy"]["order"]] . " " . $asc;

               }
           }    
           if(isset ($queryParams["palavraChave"]) && $queryParams["palavraChave"]!= "") 
           {
               $parametros["disciplina_nome"] = $queryParams["palavraChave"];
           }   
       }
       $query .= " limit :limit offset :offset ";

       $pst = $pdo->prepare($query);
       
       $pst->execute($parametros);
   
       $result = $pst->fetchAll();

       $twigParams["paginacao"] = array();
       
       @$twigParams["paginacao"]["quantidadePagina"] = ceil($result[0]["total"] / $limit);
       $twigParams["paginacao"]["atual"] = ceil($offset  / $limit) + 1;
       @$twigParams["paginacao"]["fim"] = min(($offset + $limit), $result[0]["total"]);
       @$twigParams["paginacao"]["total"] = $result[0]["total"];
       $twigParams["paginacao"]["limite"] = $limit;
                 
       $twigParams["result"] = $result;
       $twigParams["gridOpcoes"] = 
       '
         <a class="edit opcaoGrid" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons">&#xE254;</i></a>
        ';
       // <a class="view opcaoGrid" title="" data-toggle="tooltip" data-original-title="View"><i class="material-icons">&#xE417;</i></a>
       // <a class="delete opcaoGrid" title="" data-toggle="tooltip" data-original-title="Delete"><i class="material-icons">&#xE872;</i></a>    
                

       return $twigParams;
   }

   public function getDadosDisciplina($id)
   {
       $id = array("id" => $id);
       //this::with("CursoGrau")
       return $this
           ->with("Departamento", "departamento") 
           ->find($id)
           
       ;
   }   
   
   public function getDisciplinaAutoComplete($params)
   {
  
       return array
       (
           'list' =>
            $this
                ->select("disciplina", "nome as label")
                ->where('nome', 'ilike','%'.  ($params["termo"]) . '%')
                ->limit($params["limite"])
                ->get()
            
        );

   }

   public function salvarDisciplina($disciplina)
   {
        $disciplina["carga_horaria_total"] = $disciplina["carga_horaria_total"] == "" ? null : $disciplina["carga_horaria_total"];
        $disciplina["carga_horaria_teorica"] = $disciplina["carga_horaria_teorica"] == "" ? null : $disciplina["carga_horaria_teorica"];
        $disciplina["carga_horaria_pratica"] = $disciplina["carga_horaria_pratica"] == "" ? null : $disciplina["carga_horaria_pratica"];
        $disciplina["carga_horaria_distancia"] = $disciplina["carga_horaria_distancia"] == "" ? null : $disciplina["carga_horaria_distancia"];
        $disciplina["departamento"] = $disciplina["departamento"] == "" ? null : $disciplina["departamento"];
        
       if(isset($disciplina["disciplina"]) && $disciplina["disciplina"] != "" )
        {
            $sucesso = $this       
                ->where('disciplina', $disciplina["disciplina"])
                ->update
                (
                    array
                    (
                        'disciplina' => $disciplina["disciplina"],
                        'nome' => $disciplina["nome"],
                        'departamento' => $disciplina["departamento"],
                        'carga_horaria_total' => $disciplina["carga_horaria_total"],
                        'carga_horaria_teorica' => $disciplina["carga_horaria_teorica"],
                        'carga_horaria_pratica' => $disciplina["carga_horaria_pratica"],
                        'carga_horaria_distancia' => $disciplina["carga_horaria_distancia"],
                        'ativo' => (isset($disciplina["ativo"]) && $disciplina["ativo"]== true) ? true : false
                       
                    )
                )
            ;
        }    
        else
        {
            $sucesso = $this       
                ->insert
                (
                    array
                    (
                        'nome' => $disciplina["nome"],
                        'departamento' => $disciplina["departamento"],
                        'carga_horaria_total' => $disciplina["carga_horaria_total"],
                        'carga_horaria_teorica' => $disciplina["carga_horaria_teorica"],
                        'carga_horaria_pratica' => $disciplina["carga_horaria_pratica"],
                        'carga_horaria_distancia' => $disciplina["carga_horaria_distancia"],
                        'ativo' => (isset($disciplina["ativo"]) && $disciplina["ativo"]== true) ? true : false,
                        
                    )
                )
            ;
        }

        return array("sucesso" => $sucesso);
   } 
   
   public function removerDisciplina($id)
   {        
        $sucesso = $this       
            ->where('disciplina', $id)
            ->update(array('ativo' => "false"))
        ;  
        return array("sucesso" => $sucesso);
   }      
}

?>