<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/disciplina/class/model/Disciplina.php";
require_once $_ENV["BASE_DIR"]."/app/departamento/class/model/Departamento.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class DisciplinaController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();
     //  $this->usuario = new \App\Models\Usuario();
      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->disciplina = new \App\Models\Disciplina();
      
  }

  public  function getDisciplinaGrid(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $params["dados"] = array();
    $pdo = $this->pdo;
    $disciplina = $this->disciplina;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];    
    $return = array();
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response);
    $result = $disciplina->buscarDisciplina($pdo, $params);

    if(count($result)> 0)
    {

      $params["dados"]["disciplina"] = $result;
      $params["dados"]["departamento"] = (new \App\Models\Departamento())->getDepartamentos();
    
      return $response->getBody()->write($twig->render('/app/disciplina/template/disciplina.html.twig',$params));
    }
    else
    {
      // errorPage
    }

     return $response->getBody()->write($twig->render('/app/disciplina/template/disciplina.html.twig',$params));

  } 

  public  function getDisciplinaJson(Request $request, Response $response, array $args)
  {

    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $disciplina = $this->disciplina;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array();    


    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "disciplina");
    $result = $disciplina->buscarDisciplina($pdo, $params, $filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }
  }
  
  public  function getDisciplinaAutoComplete(Request $request, Response $response, array $args)
  {

    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $disciplina = $this->disciplina;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array(); 
    $permissoesPermitidas = array
    (
      "disciplina" => "disciplina",
      "grade" => "grade"
    );   
    $permissao = null;
    if(isset($parsedBody["origem"]) && in_array($parsedBody["origem"], $permissoesPermitidas))
    {
      $permissao = $permissoesPermitidas[$parsedBody["origem"]];
    }

    Menu::verificarPermissaoTela($this->pdo,/*$this->usuario*/3, $request, $response, $permissao);
    $result = $disciplina->getDisciplinaAutoComplete($filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }
  }

  public function getDadosDisciplina(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $disciplina = $this->disciplina;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array();
   
    $id = $args["id"];

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "disciplina");
    $result = $disciplina->getDadosDisciplina($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }

  public function salvarDisciplinaController(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $disciplina = $this->disciplina;
    $parsedBody = $request->getParsedBody();
    $dados = $parsedBody["disciplina"];
    $return = array();
   
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "disciplina");

    $result = $disciplina->salvarDisciplina($dados);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }  

  public function removerDisciplinaController(Request $request, Response $response, array $args)
  { 
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $disciplina = $this->disciplina;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();
   
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "disciplina");

    $result = $disciplina->removerDisciplina($id);

    if(count($result)> 0)
    {
      return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }  



}



?>