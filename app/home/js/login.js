$(document).ready(function()
{
    $(".btn-login-submit").bind("click", realizarLogin);
    $("#password").css('width', $("#username-email").css("width")); 
    $("#password").css('border', $("#username-email").css("border"));
    $("#password").css('margin', $("#username-email").css("margin"));
    $("#password").css('padding', $("#username-email").css("padding"));   
    $("#password").css('height', $("#username-email").css("height")); 
    // $("#username-email").css("width");
});


function realizarLogin()
{
    var login = {};
    login.login = $("#username-email").val();
    login.senha = $("#password").val();

    if(login.login == '' || login.senha == '')
    {
        $("#loginSenhaVazio").slideDown().delay( 2400 ).slideUp();
        return false;
    }
    var request = $.ajax
    ({
        method: "POST",
        url: "/academico/login",
        data: { login: login }

    });
    request.done(function( msg ) {

        var newDoc = document.open("text/html", "replace");

        newDoc.write(msg);
        newDoc.close();      
    });
}

$(document ).keypress(function(event) 
{
    if ( event.which == 13 )
    {   
        $(".btn-login-submit").click();
        return false;  
    }
});                