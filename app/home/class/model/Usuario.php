<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Usuario extends Model
{
   protected $table = 'usuario';
   protected $fillable = ["usuario", "login", "senha", "pessoa", "email", "codigo_recuperacao"];
   public $timestamps  = false;
   protected $primaryKey = "usuario";

   public function teste()
   {
       return self::all();
   }

   public function realizarLogin()
   {
        $getUsuario = $this::where(["login" =>  $this->login,"senha" => $this->senha])->get();
        if(count($getUsuario)  == 0)
        {
            $getUsuario = null;
        }

       return $getUsuario;
   }


}

?>