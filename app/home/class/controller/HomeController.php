<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/home/class/model/Usuario.php";
require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class HomeController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = $container["pdo"];
      $this->usuario = new \App\Models\Usuario();
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      
  }

  public  function main(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $layout = null;
    $params = $this->params;
    $menu = $this->menu;

    if (!isset($_SESSION['usuario'])) 
    {
      session_destroy();
      $layout = $_ENV["PAGINA_LOGIN"];
      $params["logado"] =   false;  
      
    }
    else
    {
      $layout = $_ENV["PAGINA_PRINCIPAL"];
      $params["logado"] =   true;  
    }
    

    return $response->getBody()->write($twig->render($layout, $params));
  }
  
  public  function login(Request $request, Response $response, array $args)
  {
    
    try
    {
      $dados = $request->getParsedBody()["login"];
      $usuario = $this->usuario; // ::all
      $twig = $this->twig;
      $menu = $this->menu;     
      $params = $this->params;

      if(!(isset($dados["login"]) && isset($dados["senha"]) && $dados["login"] != "" && $dados["senha"] != "") )
      {
        throw new Exception("");
      }
      
      $usuario->fill(array("login" => $dados["login"], "senha" => $dados["senha"]));

      $usuario = $usuario->realizarLogin();
 
      if(!isset($usuario))
      {
        throw new Exception("");
      }

      $_SESSION["usuario"] = $usuario[0]->usuario;

      $menu = Menu::getMenu($this->pdo, $_SESSION["usuario"]);
      $params["menu"] = $menu;
      $params["logado"] =   true;  

      $layout = $_ENV["PAGINA_PRINCIPAL"];


    }
    catch(Exception $e)
    {
      session_destroy();
      $layout = $_ENV["PAGINA_LOGIN"];
      $params["logado"] =   false;  
      $params["mensagemErro"] = true;
    }       


    return $response->getBody()->write($twig->render($layout,$params));
  }  


  public function logout (Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $layout = $_ENV["PAGINA_LOGIN"];
    session_destroy();

     return $response->withStatus(302)->withHeader('Location', '/academico/');
   // $this->container->redirect($this->container->urlFor('root') );
    //return $res->withRedirect("localhost/academico");
  }





}



?>