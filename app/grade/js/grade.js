$(document).ready(function()
{
    $("#divTabGrade").tabs();
    preencherGrade();
    preencherPaginacao();
    // preencherDepartamento();
    $("#tableGrades").find(".sortOption").bind("click", ordenarTableGrades);
    $("#buscaPalavraChave").bind("keyup",buscarGrade);    
    $("#d-periodoGrid").bind("change",buscarDisciplinasByGradePeriodo)
    //$("#c-departamento").bind("change", changeSelectDepartamento);
    $("#tableDisciplinas").find(".sortOption").bind("click", orderTableDisciplinas);
    $("#btnNovaDisciplina").bind("click", novaDisciplina);
    // salvarGradeDisciplina
    $("#btnSalvarGradeDisciplina").bind("click", salvarGradeDisciplina);
    $("#btnSalvarRequisito").bind("click",salvarRequisito);
    autoCompleteDisciplina($("#d-disciplinaNome"));
    autoCompleteCurso( $("#g-cursoNome")  );
    $("#btnNovaGrade").bind("click",novaGrade);

    $("#btnSalvarGrade").bind("click",salvarGrade);
    $("#btnCancelarGrade").bind("click",cancelarGrade);

    $("#btnNovaGradeDisciplina").bind("click",novaGradeDisciplina);
    $("#btnCancelarGradeDisciplina").bind("click",fecharFormDisciplina);
});

function autoCompleteDisciplina(jQueryObject)
{
    jQueryObject.autocomplete
    ({
        source: function (request, response) 
        {
            $.ajax({
                type: "post",
                url: "disciplina_autocomplete",
                dataType: "html",
                async: false,
                cache: false,
                data: 
                {
                    filtro: 
                    {
                        termo: request.term,
                        limite: 20,
                    },
                    origem: "grade",
                    grade: $("#g-hdnGrade").val(),
                    periodo: $("#d-periodoGrid").val()
                },
                success: function (data)
                {
                    var result = $.parseJSON(data);
                    response(result.list);
                }
            });
        },
        minLength: 0,
        select: function (event, ui) 
        {
            $(this).val(ui.item.label);
            $(this).closest(".autoComplete").find(".autoCompleteId").val(ui.item.disciplina);
            return false;
        },
        change: function(event, ui) 
        {
            if(ui.item == null || ui.item === undefined)
            {
                $(this).val("");
                $(this).closest(".autoComplete").find(".autoCompleteId").val("");
            }
            return false;
        }
    }, 
    {
        selectFirst: true,
        minLength: 0
    });
}

function ordenarTableGrades()
{
    var tableGrades = $("#tableGrades");
    var filtro = $(this);

    orderSort(tableGrades, filtro);
    
    buscarGrade();
    preencherPaginacao();      
}

function orderTableDisciplinas()
{
    var table = $("#tableDisciplinas");
    var filtro = $(this);

    orderSort(table, filtro);
    
    buscarDisciplinasByGradePeriodo();

}
function orderSort(table, filtro)
{
    var filtro = filtro;
    
    $(table).find(".sortOption").find(".fa").not
        (
            function()
            {
                return $(filtro).data("sort") == $(this).closest(".sortOption").data("sort") ? true : false;
            }
        )
        .addClass("fa-sort")
        .removeClass("fa-sort-down")
        .removeClass("fa-sort-up")
        .removeClass("selected")
    ;

    if(filtro.find(".fa").hasClass("fa-sort-down") == true)
    {      
        filtro.find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-down")
            .addClass("fa-sort-up")
            .addClass("selected")
        ;
    }
    else
    {   
        filtro.find(".fa")
            .removeClass("fa-sort")
            .removeClass("fa-sort-up")
            .addClass("fa-sort-down")
            .addClass("selected")
        ;
    }
    
  
}

function buscarGrade(event = null)
{

    var filtro = {};
    filtro.orderBy = {};
    filtro.orderBy.order = $("#tableGrades").find(".sortOption").find("i").filter(".selected").closest(".sortOption").attr("data-sort");
    filtro.orderBy.asc =  $("#tableGrades").find(".sortOption").find("i").filter(".selected").filter(".fa-sort-up").length > 0 ? "asc" : "desc";
    filtro.palavraChave = $("#buscaPalavraChave").val();
    filtro.pagina = 1;

    if(event !=  null)
    if(event.data != null && event.data != undefined)
    {
        filtro.pagina = event.data.pagina;
    }

    $.ajax
    ({
        url: "grade",
        method: "POST",
        dataType: "html",
        data: 
        {
            filtro: filtro
        },
        success: function(data)
        {
            var result = JSON.parse(data);

            $("#hdnGrades").data("dados", result.result);
            $("#hdnPaginacao").data("dados", result.paginacao);
            preencherGrade();
            preencherPaginacao();

            $("#tableDisciplinas").find(".sortOption").find(".fa")
                .addClass("fa-sort")
                .removeClass("fa-sort-down")
                .removeClass("fa-sort-up")
                .removeClass("selected")
            ;            
        }
    });


}

function preencherPaginacao()
{

    var paginacao = $("#hdnPaginacao").data("dados");
    var desabilitarPrimeiraPagina = (paginacao.atual == null || paginacao.atual == 1) ? "disabled" : ""; 
    var desabilitarUltimaPagina = (paginacao.atual == null || paginacao.atual == paginacao.quantidadePagina) ? "disabled" : ""; 
    
    $(".pagination").empty();
    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarPrimeiraPagina + "'>").append
        (
            '<a ><i class="fa fa-angle-double-left"></i></a>'
        ).bind("click", {pagina: 1}, buscarGrade)
    );

    var i;
    var limite = paginacao.quantidadePagina == null ? 1 : paginacao.quantidadePagina;
    for(i = 1;i<= limite; i++)
    {
        $(".pagination").append
        (
            $("<li class='page-item lipg-"  + i + "'>").append
            (
                '<a  class="page-link">'+ i +'</a>'
            ).bind("click", {pagina: i}, buscarGrade)           
        );
    }

    $(".pagination").append
    (
        $("<li class='page-item " + desabilitarUltimaPagina + "'>").append
        (
            '<a ><i class="fa fa-angle-double-right"></i></a>'
        ).bind("click", {pagina: paginacao.quantidadePagina}, buscarGrade)
    );    

    $(".lipg-"+ paginacao.atual).addClass("active");

}

function preencherGrade(event = null)
{    
    var disciplinas = $("#hdnGrades").data("dados");
    $("#tbodyGrades").empty();
    $(disciplinas).each(function(index, elem)
    {

        $("#tbodyGrades").append
        (
            
            $("<tr id=trId_'" + elem.grade + "' data-id='"+elem.grade+"'> ")
                .append
                (
                    $("<td colspan='1'>").append(elem.grade)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.grade_nome)
                )                	
                .append
                (
                    $("<td colspan='1'>").append(elem.curso_nome)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.periodo_total)
                )                
                .append
                (
                    $("<td colspan='1'>").append(elem.carga_horaria_letiva)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.carga_horaria_trabalho_final)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.ativo)
                )


                .append
                (
                    $("<td colspan='1'>").html(JSON.parse($("#hdnGridOpcoes").data("dados")))
                        
                )
        );
        
    
    });

    $("#tableGrades").find(".opcaoGrid").filter(".edit").on("click", editGrade);
   
}
          
function editGrade()
{
    var botao = $(this);
    var id = $(botao).closest("tr").attr("data-id");    

    $.ajax
    ({
        url: "grade/" + id,
        method: "GET",
        dataType: "html",
        data: 
        {
            id: id
        },
        success: function(data)
        {
            var result = JSON.parse(data);
            $("#tbodyDisciplina").find('option').not(':first').remove();

            if(result.length > 0)
            {
                resetarFormulario($("#tabDisciplina"));
                resetarFormulario($("#tabRequisito"));
                resetarFormulario($("#tabGrade"));

                $("#g-hdnGrade").val(result[0].grade);
                $("#g-gradeNome").val(result[0].nome);
                $("#g-cursoNome").val(result[0].curso.nome);
                $("#g-cursoId").val(result[0].curso.curso);
                $("#g-periodoTotal").val(result[0].periodo_total);
                $("#g-cargaHorariaLetiva").val(result[0].carga_horaria_letiva);
                $("#g-cargaHorariaComplementar").val(result[0].carga_horaria_complementar);
                $("#g-cargaHorariaEstagio").val(result[0].carga_horaria_estagio);
                $("#g-cargaHorariaTrabalhoFinal").val(result[0].carga_horaria_trabalho_final);
                $("#g-anoSemestre").val((result[0].ano_inicio + "/" + result[0].semestre_inicio));

                $("#g-ativo").val(result[0].ativo == true ? "true" : "false");

                 preencherSelectPeriodo(result[0].periodo_total);


                $("#divTabGrade").show();
                $("#divTabGrade").tabs({active: 0});
                $(".formSalvarDisciplina").hide();


            }

        }
    });
}          

function preencherSelectPeriodo(total)
{

    var i = 1;
    $(".d-periodo").val("");
   // $(".d-periodo").find('option').not(':first').remove();
    $("#d-periodoGrid").find('option').not(':first').remove();
    $("#d-periodoForm").find('option').not(':first').remove();
    for(i; i <=  total; i++ )
    {
        
        $(".d-periodo").append
        (
            $("<option>", 
            {
                text: i,
                value: i
            })
        );
    }
}

function buscarDisciplinasByGradePeriodo()
{
    var periodo = $("#d-periodoGrid").val();
    var grade =  $("#g-hdnGrade").val();

    if(grade != "" && periodo != "")
    {
        var filtro = {};
        filtro.periodo = periodo;
        filtro.grade = grade;
        filtro.orderBy = {};
        filtro.orderBy.order = $("#tableDisciplinas").find(".sortOption").find("i").filter(".selected").closest(".sortOption").attr("data-sort");
        filtro.orderBy.asc =  $("#tableDisciplinas").find(".sortOption").find("i").filter(".selected").filter(".fa-sort-up").length > 0 ? "asc" : "desc";
      
        $.ajax
        ({
            url: "grade_disciplina",
            method: "POST",
            dataType: "html",
            data: 
            {
                filtro: filtro
            },
            success: function(data)
            {
                var result = JSON.parse(data);

                if(result.result )
                {
                    $("#tbodyDisciplinas").empty();
                    preencherDisciplinas(result.result, result.gridOpcoes);
                }
            }
        });        
    }
}

function preencherDisciplinas(dados, opcoes)
{

    $(dados).each(function(index, elem)
    {
        $("#tbodyDisciplinas").append
        (            
            $("<tr id=trId_disciplina_'" + elem.grade_disciplina + "' data-id='"+elem.grade_disciplina+"'> ")
                .append
                (
                    $("<td colspan='1'>").append(elem.grade_disciplina)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.disciplina_nome)
                )                	
                .append
                (
                    $("<td colspan='1'>").append(elem.periodo)
                )  
                .append
                (
                    $("<td colspan='1'>").html(opcoes)
                )
        );              
    });

    $("#tableDisciplinas").find(".opcaoGrid").filter(".edit").on("click", editarDisciplina);
    $("#tableDisciplinas").find(".opcaoGrid").filter(".delete").on("click", removerGradeDisciplina);
}

function editarDisciplina()
{

    var id = $(this).closest("tr").attr("data-id");    
    if(id != "")
    {
        $.ajax
        ({
            url: "disciplina_grade",
            method: "post",
            dataType: "html",
            data: 
            {
                id: id
            },
            success: function(data)
            {
                var result = JSON.parse(data);

                resetarFormulario($("#tabDisciplina"));
                $(".formSalvarDisciplina").show();            

                $("#d-periodoForm").val(result[0].periodo);
                $("#d-disciplinaId").val(result[0].disciplina.disciplina);
                $("#d-disciplinaNome").val(result[0].disciplina.nome);
                $("#d-hdnGradeDisciplinaId").val(result[0].grade_disciplina);

                buscarGradeRequisitoDisciplinaGridByGradeDisciplina();
                
            }
        });     
    }
}

function buscarGradeRequisitoDisciplinaGridByGradeDisciplina()
{
    var gradeDisciplina =  $("#d-hdnGradeDisciplinaId").val();

    if(gradeDisciplina != "" )
    {
        var filtro = {};
        filtro.gradeDisciplina = gradeDisciplina;
        filtro.orderBy = {};
        filtro.orderBy.order = $("#tableRequisitos").find(".sortOption").find("i").filter(".selected").closest(".sortOption").attr("data-sort");
        filtro.orderBy.asc =  $("#tableRequisitos").find(".sortOption").find("i").filter(".selected").filter(".fa-sort-up").length > 0 ? "asc" : "desc";
      
        $.ajax
        ({
            url: "grade_requisito",
            method: "POST",
            dataType: "html",
            data: 
            {
                filtro: filtro
            },
            success: function(data)
            {
                var result = JSON.parse(data);

                $("#tbodyRequisitos").empty();
                preencherRequisitos(result.result, result.gridOpcoes);

            }
        });        
    }
}

function preencherRequisitos(dados, opcoes)
{

    $(dados).each(function(index, elem)
    {
        $("#tbodyRequisitos").append
        (            
            $("<tr id=trId_requisito_'" + elem.grade_disciplina + "' data-id='"+elem.grade_disciplina_requisito+"'> ")
                .append
                (
                    $("<td colspan='1'>").append(elem.grade_disciplina_requisito)
                )
                .append
                (
                    $("<td colspan='1'>").append(elem.disciplina_nome)
                )                	
                .append
                (
                    $("<td colspan='1'>").append(elem.disciplina_periodo)
                )  
                .append
                (
                    $("<td colspan='1'>").append(elem.disciplina_tipo)
                )                  
                .append
                (
                    $("<td colspan='1'>").html(opcoes)
                )
        );              
    });

    $("#tbodyRequisitos").find(".opcaoGrid").filter(".edit").on("click", editarRequisito);
    $("#tbodyRequisitos").find(".opcaoGrid").filter(".delete").on("click", removerRequisito);
}

function editarRequisito()
{
    var id = $(this).closest("tr").attr("data-id");    
    if(id != "")
    {
        $.ajax
        ({
            url: "grade_requisito_get",
            method: "post",
            dataType: "html",
            data: 
            {
                id: id
            },
            success: function(data)
            {
                var result = JSON.parse(data);
                $("#r-hdnGradeDisciplinaRequisitoId").val(result[0].grade_disciplina_requisito);
                $("#r-disciplinaNome").val(result[0].requisito_nome);
                $("#r-disciplinaId").val(result[0].requisito_grade_disciplina);
                $("#r-requisitoTipo").val(result[0].requisito_tipo);
                 $("#r-periodo").val(result[0].requisito_periodo);
                console.log(result);
                
            }
        });     
    }
}

function removerRequisito()
{

    var id = $(this).closest("tr").data("id");
    
    $.ajax
    ({
        url: "grade_requisito_remover",
        method: "post",
        dataType: "html",
        data: 
        {
            id: id
        },
        success: function(data)
        {
            var result = JSON.parse(data);
            console.log(result);
        }
    });  

}



function novaDisciplina()
{
    $("#d-periodoForm").val("");
    $("#d-disciplinaId").val("");
    $("#d-disciplinaNome").val("");
    $("#hdnGradeDisciplinaId").val("");
}

function salvarGradeDisciplina()
{
 
    var gradeDisciplina = {};
    
    gradeDisciplina.periodo = $("#d-periodoForm").val();
    gradeDisciplina.disciplina = $("#d-disciplinaId").val();
    gradeDisciplina.gradeDisciplina =  $("#d-hdnGradeDisciplinaId").val();
    gradeDisciplina.grade =  $("#g-hdnGrade").val();

    $.ajax
    ({
        url: "disciplina_grade_salvar",
        method: "post",
        dataType: "html",
        data: 
        {
            gradeDisciplina: gradeDisciplina
        },
        success: function(data)
        {
            var result = JSON.parse(data);

            $(".formSalvarDisciplina").show();
           
        }
    });     

}


function salvarRequisito()
{
 
    var requisito = {};
    
    requisito.gradeDisciplinaRequisito =  $("#r-hdnGradeDisciplinaRequisitoId").val();
    requisito.gradeDisciplina = $("#d-hdnGradeDisciplinaId").val();
    requisito.requisito =  $("#r-disciplinaId").val();
    requisito.requisitoTipo =  $("#r-requisitoTipo").val();

    $.ajax
    ({
        url: "grade_requisito_salvar",
        method: "post",
        dataType: "html",
        data: 
        {
            requisito: requisito
        },
        success: function(data)
        {
            var result = JSON.parse(data);
        }
    });     

}




function removerGradeDisciplina()
{
    var id = $(this).closest("tr").data("id");
    
    $.ajax
    ({
        url: "disciplina_grade_remover",
        method: "post",
        dataType: "html",
        data: 
        {
            id: id
        },
        success: function(data)
        {
            var result = JSON.parse(data);
            buscarDisciplinasByGradePeriodo();
       
            fecharFormDisciplina();
            exibirAlerta("Disciplina removida da grade com sucesso.","Sucesso", $(".msgAlert"));            
        }
    });  
}

function salvarGrade()
{
    // TODO
    var splitAnoSemestre = ($("#g-anoSemestre").val()).split("/");
    var grade = {};
    
    grade.grade = $("#g-hdnGrade").val();
    grade.curso = $("#g-cursoId").val();
    grade.grade_nome = $("#g-gradeNome").val();
    grade.periodo_total = $("#g-periodoTotal").val();
    // grade.carga_horaria_letiva = $("#montar").val();
    grade.carga_horaria_complementar = $("#g-cargaHorariaComplementar").val();
    grade.carga_horaria_estagio = $("#g-cargaHorariaEstagio").val();
    grade.carga_horaria_trabalho_final = $("#g-cargaHorariaTrabalhoFinal").val();
	grade.ano_inicio = splitAnoSemestre[0];
	grade.semestre_inicio = splitAnoSemestre[1];
	grade.ativo = $("#g-ativo").val();

    $.ajax
    ({
        url: "grade_salvar",
        method: "post",
        dataType: "html",
        data: 
        {
            grade: grade
        },
        success: function(data)
        {
            var result = JSON.parse(data);

            exibirAlerta("Grade salva com sucesso.","Sucesso", $(".msgAlert"));
               
            ordenarTableGrades();
           
        }
    });     

}

function removerGrade()
{
    var id = $(this).closest("tr").data("id");
    
    $.ajax
    ({
        url: "grade_remover",
        method: "post",
        dataType: "html",
        data: 
        {
            id: id
        },
        success: function(data)
        {
            var result = JSON.parse(data);
            console.log(result);
        }
    });  
}

function novaGrade()
{
    resetarFormulario($("#tabDisciplina"));
    resetarFormulario($("#tabRequisito"));
    resetarFormulario($("#tabGrade"));

    $(".formSalvarDisciplina").hide();

     $("#divTabGrade").show();
     $("#divTabGrade").tabs({active: 0});   

}

function autoCompleteCurso(jQueryObject)
{
    jQueryObject.autocomplete
    ({
        source: function (request, response) 
        {
            $.ajax({
                type: "post",
                url: "cursos_autocomplete",
                dataType: "html",
                async: false,
                cache: false,
                data: 
                {
                    filtro: 
                    {
                        termo: request.term,
                        limite: 20,
                    },
                    origem: "cursos"

                },
                success: function (data)
                {
                    var result = $.parseJSON(data);
                    response(result.list);
                }
            });
        },
        minLength: 0,
        select: function (event, ui) 
        {
            $(this).val(ui.item.label);
            $(this).closest(".autoComplete").find(".autoCompleteId").val(ui.item.curso);
            return false;
        },
        change: function(event, ui) 
        {
            if(ui.item == null || ui.item === undefined)
            {
                $(this).val("");
                $(this).closest(".autoComplete").find(".autoCompleteId").val("");
            }
            return false;
        }
    }, 
    {
        selectFirst: true,
        minLength: 0
    });
}

function cancelarGrade()
{
    $("#divTabGrade").hide();
}

function novaGradeDisciplina()
{
    resetarFormulario($("#tabDisciplina"));
    $(".formSalvarDisciplina").show();

}

function fecharFormDisciplina()
{
    $(".formSalvarDisciplina").hide();
}