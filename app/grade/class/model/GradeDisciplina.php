<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;


class GradeDisciplina extends Model
{
   protected $table = 'grade_disciplina';

   protected $fillable = 
   [    
    "grade_disciplina", "grade", "disciplina", "periodo", "disciplina_tipo", "ativo"
   ];
   public $timestamps  = false;
   protected $primaryKey = "grade_disciplina";

   public function Grade()
   {
       return $this->belongsTo("\App\Models\Grade","grade");
   }

   public function Disciplina()
   {
       return $this->belongsTo("\App\Models\Disciplina", "disciplina");
   }

   public function getDisciplinaGradeById($id)
   {
       $id = array("id" => $id);
 
       return $this
           ->with("Disciplina", "disciplina")
           ->with("Grade", "grade") 
           ->find($id)           
       ;
   }   

   public function removerDisciplinaGradeById($id)
   {

        $gradeDisciplina = $this->find($id);
        
        $sucesso = $this       
            ->where('grade_disciplina', $id)
            ->update(array('ativo' => "false"))
        ;
        //echo (gettype($sucesso));
        //echo json_encode($sucesso);
        return array("sucesso" => $sucesso);
        // return $sucesso;      
   }     
   
   public function salvarGradeDisciplina($gradeDisciplina)
   {
        if(isset($gradeDisciplina["gradeDisciplina"]) && $gradeDisciplina["gradeDisciplina"] != "" )
        {
            $sucesso = $this       
                ->where('grade_disciplina', $gradeDisciplina["gradeDisciplina"])
                ->update
                (
                    array
                    (
                        'disciplina' => $gradeDisciplina["disciplina"],
                        'periodo' => $gradeDisciplina["periodo"],
                        'grade' => $gradeDisciplina["grade"],
                        "ativo"=> "true",
                        "disciplina_tipo" => 1
                    )
                )
            ;
        }    
        else
        {
            $sucesso = $this       
                ->insert
                (
                    array
                    (
                        'disciplina' => $gradeDisciplina["disciplina"],
                        'periodo' => $gradeDisciplina["periodo"],
                        'grade' => $gradeDisciplina["grade"],
                        "ativo"=> "true",
                        "disciplina_tipo" => 1
                    )
                )
            ;
        }

        return array("sucesso" => $sucesso);
   }      
}

?>