<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
   protected $table = 'grade';



   protected $fillable = 
   [    
       "grade", "curso", "nome", "periodo_total", "carga_horaria_letiva",
        "carga_horaria_complementar", "carga_horaria_estagio", "carga_horaria_trabalho_final",
        "ano_inicio", "semestre_inicio", "ativo"
   ];
   public $timestamps  = false;
   protected $primaryKey = "grade";

   public function Curso()
   {
       return $this->belongsTo("\App\Models\Curso","curso");
   }

   public function buscarGrade($pdo, $twigParams, $queryParams = null)
   {
       
       $limit = $queryParams["paginacao"]["limit"] ?? 10;
       $pagina = !isset($queryParams["pagina"]) ? 1 : $queryParams["pagina"];

       $offset = ($pagina - 1)  * $limit;

       $parametros = array("grade_nome" => "", "limit" => $limit, "offset" => $offset);

       $orderByPermitidos = array
       (
           "grade" => "grade",
           "grade_nome" => "grade_nome",
           "curso_nome" => 'curso_nome',
           "periodo_total" => "periodo_total",
           "departamento_nome" => "departamento_nome", 
           "departamento" => "departamento", 
           "ativo" => "ativo", 
           "carga_horaria_letiva" => "carga_horaria_letiva",
           "carga_horaria_trabalho_final" => "carga_horaria_trabalho_final"
           
       );

       $query = 
       "
        select 
            g.grade, 
            g.curso, 
            g.nome grade_nome, 
            g.periodo_total, 
            g.carga_horaria_letiva,
            g.carga_horaria_trabalho_final,
            case when g.ativo = true then 'Ativo' else 'Inativo' end ativo,
            c.nome curso_nome,
            count(*) over() total
        from 
            grade g
            inner join curso c on g.curso = c.curso
         where
           g.nome ilike '%' || :grade_nome || '%' 
         
       ";
       if(is_array($queryParams))
       {
           if(array_key_exists("orderBy", $queryParams))
           {
               if(isset($queryParams["orderBy"]["order"]) && in_array($queryParams["orderBy"]["order"], $orderByPermitidos))
               {
                   $asc = (isset($queryParams["orderBy"]["asc"]) && $queryParams["orderBy"]["asc"] == "asc") ? "asc" : "desc";
                   $query .= " order by ". $orderByPermitidos[$queryParams["orderBy"]["order"]] . " " . $asc;

               }
           }    
           if(isset ($queryParams["palavraChave"]) && $queryParams["palavraChave"]!= "") 
           {
               $parametros["grade_nome"] = $queryParams["palavraChave"];
           }   
       }
       $query .= " limit :limit offset :offset ";

       $pst = $pdo->prepare($query);
       
       $pst->execute($parametros);
   
       $result = $pst->fetchAll();

       $twigParams["paginacao"] = array();
       
       @$twigParams["paginacao"]["quantidadePagina"] = ceil($result[0]["total"] / $limit);
       $twigParams["paginacao"]["atual"] = ceil($offset  / $limit) + 1;
       @$twigParams["paginacao"]["fim"] = min(($offset + $limit), $result[0]["total"]);
       @$twigParams["paginacao"]["total"] = $result[0]["total"];
       $twigParams["paginacao"]["limite"] = $limit;
                
       $twigParams["result"] = $result;
       $twigParams["gridOpcoes"] = 
       '
          <a class="edit opcaoGrid" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons">&#xE254;</i></a>
        ';

       return $twigParams;
   }

   public function buscarDisciplinaGrade($pdo, $twigParams, $queryParams = null)
   {


        $parametros = array
        (
            "disciplina_nome" => "",
            "periodo" => $queryParams["periodo"],
            "grade" =>$queryParams["grade"] 
        );

       $orderByPermitidos = array
       (
           "grade_disciplina" => "grade_disciplina",
           "disciplina_nome" => "disciplina_nome",
           "periodo" => 'periodo'
       );

       $query = 
       "
            select 	
                gd.grade_disciplina,
                d.nome disciplina_nome,
                gd.periodo
            from 
                grade_disciplina gd
                inner join grade g on gd.grade = g.grade
                inner join disciplina d on gd.disciplina = d.disciplina
            where
                g.grade = :grade and gd.periodo = :periodo
                and  d.nome ilike '%' || :disciplina_nome || '%' 
                and gd.ativo = true
                    
       ";
       if(is_array($queryParams))
       {
           if(array_key_exists("orderBy", $queryParams))
           {
               if(isset($queryParams["orderBy"]["order"]) && in_array($queryParams["orderBy"]["order"], $orderByPermitidos))
               {
                   $asc = (isset($queryParams["orderBy"]["asc"]) && $queryParams["orderBy"]["asc"] == "asc") ? "asc" : "desc";
                   $query .= " order by ". $orderByPermitidos[$queryParams["orderBy"]["order"]] . " " . $asc;

               }
           }    
           if(isset ($queryParams["palavraChave"]) && $queryParams["palavraChave"]!= "") 
           {
               $parametros["disciplina_nome"] = $queryParams["palavraChave"];
           }   
       }
       // $query .= " limit :limit offset :offset ";

       $pst = $pdo->prepare($query);
       
       $pst->execute($parametros);
   
       $result = $pst->fetchAll();
            
       $twigParams["result"] = $result;
       $twigParams["gridOpcoes"] = 
       '
         <a class="edit opcaoGrid" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons">&#xE254;</i></a>
         <a class="delete opcaoGrid" title="" data-toggle="tooltip" data-original-title="Delete"><i class="material-icons">&#xE872;</i></a>    
       ';

       return $twigParams;       
   }

   public function getDadosGrade($id)
   {
       $id = array("id" => $id);
       //this::with("CursoGrau")
       return $this
           ->with("Curso", "curso") 
           ->find($id)
           
       ;
   }  
   
   public function salvarGrade($grade)
   {
        if(isset($grade["grade"]) && $grade["grade"] != "" )
        {
            $sucesso = $this       
                ->where('grade', $grade["grade"])
                ->update
                (
                    array
                    (
                        'curso' => $grade["curso"],
                        'nome' => $grade["grade_nome"],
                        'periodo_total' => $grade["periodo_total"],
                        // 'carga_horaria_letiva' => $grade["disciplina"],
                        'carga_horaria_complementar' => $grade["carga_horaria_complementar"],
                        'carga_horaria_estagio' => $grade["carga_horaria_estagio"],
                        'carga_horaria_trabalho_final' => $grade["carga_horaria_trabalho_final"],
                        'ano_inicio' => $grade["ano_inicio"],
                        'semestre_inicio' => $grade["semestre_inicio"],
                        'ativo' => isset($grade["ativo"]) && $grade["ativo"] == true ? true : false
                    )
                )
            ;
        }    
        else
        {
            $sucesso = $this       
                ->insert
                (
                    array
                    (
                        'curso' => $grade["curso"],
                        'nome' => $grade["grade_nome"],
                        'periodo_total' => $grade["periodo_total"],
                        'carga_horaria_letiva' => 0,
                        'carga_horaria_complementar' => $grade["carga_horaria_complementar"],
                        'carga_horaria_estagio' => $grade["carga_horaria_estagio"],
                        'carga_horaria_trabalho_final' => $grade["carga_horaria_trabalho_final"],
                        'ano_inicio' => $grade["ano_inicio"],
                        'semestre_inicio' => $grade["semestre_inicio"],
                        'ativo' => isset($grade["ativo"]) && $grade["ativo"] == true ? true : false
                    )
                )
            ;
        }

        return array("sucesso" => $sucesso);
   } 
   
   public function removerGrade($id)
   {        
        $sucesso = $this       
            ->where('grade', $id)
            ->update(array('ativo' => "false"))
        ;  
        return array("sucesso" => $sucesso);
   }     
}

?>