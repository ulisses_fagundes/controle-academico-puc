<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;


class GradeDisciplinaRequisito extends Model
{
   protected $table = 'grade_disciplina_requisito';

   protected $fillable = 
   [    
        "grade_disciplina_requisito", "grade_disciplina", "requisito", "requisito_tipo"
   ];
   public $timestamps  = false;
   protected $primaryKey = "grade_disciplina_requisito";

   public function GradeDisciplina()
   {
       return $this->belongsTo("\App\Models\GradeDisciplina","grade_disciplina");
   }

   public function Requisito()
   {
       return $this->belongsTo("\App\Models\GradeDisciplina", "requisito");
   }

   public function getRequisitoGrid($pdo, $twigParams, $queryParams = null)
   {
        $parametros = array
        (
            "disciplina_nome" => "",
            "grade_disciplina" => $queryParams["gradeDisciplina"]
        );

        $orderByPermitidos = array
        (
            "grade_disciplina_requisito" => "grade_disciplina_requisito",
            "disciplina_tipo" => "disciplina_tipo",
            "disciplina_periodo" => 'disciplina_periodo',
            "disciplina_nome" => 'disciplina_nome'
        );

        $query = 
        "
                select 
                    gdr.grade_disciplina_requisito,
                    gdr.requisito, 
                    gdr.requisito_tipo disciplina_tipo,
                    r.grade_disciplina,
                    r.disciplina_tipo,
                    r.periodo disciplina_periodo,
                    rd.nome disciplina_nome
                from 
                    grade_disciplina_requisito gdr
                    inner join grade_disciplina gd on gdr.grade_disciplina = gd.grade_disciplina
                    inner join disciplina d on gd.disciplina = d.disciplina
                    
                    inner join grade_disciplina r on gdr.requisito = r.grade_disciplina
                    inner join disciplina rd on rd.disciplina = r.disciplina
                where
                    gdr.grade_disciplina = :grade_disciplina
                    and  rd.nome ilike '%' || :disciplina_nome || '%' 
                   
                        
        ";
        if(is_array($queryParams))
        {
            if(array_key_exists("orderBy", $queryParams))
            {
                if(isset($queryParams["orderBy"]["order"]) && in_array($queryParams["orderBy"]["order"], $orderByPermitidos))
                {
                    $asc = (isset($queryParams["orderBy"]["asc"]) && $queryParams["orderBy"]["asc"] == "asc") ? "asc" : "desc";
                    $query .= " order by ". $orderByPermitidos[$queryParams["orderBy"]["order"]] . " " . $asc;

                }
            }    
            if(isset ($queryParams["palavraChave"]) && $queryParams["palavraChave"]!= "") 
            {
                $parametros["disciplina_nome"] = $queryParams["palavraChave"];
            }   
        }

        $pst = $pdo->prepare($query);
        
        $pst->execute($parametros);

        $result = $pst->fetchAll();
                    
        $twigParams["result"] = $result;
        $twigParams["gridOpcoes"] = 
        '
            <a class="view opcaoGrid" title="" data-toggle="tooltip" data-original-title="View"><i class="material-icons">&#xE417;</i></a>
            <a class="edit opcaoGrid" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons">&#xE254;</i></a>
            <a class="delete opcaoGrid" title="" data-toggle="tooltip" data-original-title="Delete"><i class="material-icons">&#xE872;</i></a>    
        ';

        return $twigParams;       
   }   

   public function getGradeDisciplinaRequisitoById($id)
   {
       $id = array("id" => $id);
 
       return $this

           ->join("grade_disciplina as requisito", "grade_disciplina_requisito.requisito", "=", "requisito.grade_disciplina") 
           ->join("disciplina as requisito_disciplina","requisito.disciplina", "=","requisito_disciplina.disciplina")

           ->select
            (
                    "grade_disciplina_requisito","requisito_disciplina.nome as requisito_nome", 
                    "requisito_tipo", "requisito.grade_disciplina as requisito_grade_disciplina",
                    "requisito.periodo as requisito_periodo"
            )
           ->find($id)           
       ;
   }   

   public function removerRequisito($id)
   {

        $sucesso = $this       
            ->where('grade_disciplina_requisito', $id)
            ->delete()
        ;

        return array("sucesso" => $sucesso);   
   }     
   
   public function salvarRequisito($requisito)
   {
        if(isset($requisito["gradeDisciplinaRequisito"]) && $requisito["gradeDisciplinaRequisito"] != "" )
        {
            $sucesso = $this       
                ->where('grade_disciplina_requisito', $requisito["gradeDisciplinaRequisito"])
                ->update
                (
                    array
                    (
                        'grade_disciplina' => $requisito["gradeDisciplina"],
                        'requisito' => $requisito["requisito"],
                        'requisito_tipo' => $requisito["requisitoTipo"]
                    )
                )
            ;
        }    
        else
        {
            $sucesso = $this       
                ->insert
                (
                    array
                    (
                        'grade_disciplina' => $requisito["gradeDisciplina"],
                        'requisito' => $requisito["requisito"],
                        'requisito_tipo' => $requisito["requisitoTipo"]
                    )
                )
            ;
        }

        return array("sucesso" => $sucesso);
   }      
}

?>