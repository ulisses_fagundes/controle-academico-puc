<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use EloquentConfig\EloquentConfig as EloquentConfig;

require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/grade/class/model/Grade.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";



class GradeController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();
      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->grade = new \App\Models\Grade();
      
  }

  public  function getGradeGrid(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $params["dados"] = array();
    $pdo = $this->pdo;
    $grade = $this->grade;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];    
    $return = array();
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $grade->buscarGrade($pdo, $params);

    if(count($result)> 0)
    {

      $params["dados"]["grade"] = $result;
    
      return $response->getBody()->write($twig->render('/app/grade/template/grade.html.twig',$params));
    }
    else
    {
      // errorPage
    }

     return $response->getBody()->write($twig->render('/app/grade/template/grade.html.twig',$params));

  } 

  public  function getGradeJson(Request $request, Response $response, array $args)
  {

    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $grade = $this->grade;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array();    


    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $grade->buscarGrade($pdo, $params, $filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }



  }  

  public function getDadosGrade(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $grade = $this->grade;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array();
   
    $id = $args["id"];

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $grade->getDadosGrade($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }

  public function getDisciplinaGrade(Request $request, Response $response, array $args)
  {
    $usuario = $this->usuario;
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $grade = $this->grade;
    $parsedBody = $request->getParsedBody();
    $filtro = isset($parsedBody["filtro"])  ? $parsedBody["filtro"] : null;
    $return = array();    

    if(!isset($filtro["grade"]) || $filtro["grade"] == "" || !isset($filtro["periodo"]) || $filtro["periodo"] == ""  )
    {
      return false;
    }

    Menu::verificarPermissaoTela($this->pdo,/*3*/ $usuario, $request, $response, "grade");
    $result = $grade->buscarDisciplinaGrade($pdo, $params, $filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }  

  public function salvarGradeController(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $grade = $this->grade;
    $parsedBody = $request->getParsedBody();
    $dados = $parsedBody["grade"];
    $return = array();

    
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");

    $result = $grade->salvarGrade($dados);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }  

  public function removerGradeController(Request $request, Response $response, array $args)
  { 
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $grade = $this->grade;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();
   
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $grade->removerGrade($id);
    if(count($result)> 0)
    {
      return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }  




}
?>