<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/grade/class/model/GradeDisciplina.php";
require_once $_ENV["BASE_DIR"]."/app/grade/class/controller/GradeDisciplinaRequisitoController.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class GradeDisciplinaController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;
  private $gradeDisciplina;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();
     //  $this->usuario = new \App\Models\Usuario();
      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->gradeDisciplina = new \App\Models\GradeDisciplina();
      
  }

  public function removerDisciplinaGradeById(Request $request, Response $response, array $args)
  {
 
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplina = $this->gradeDisciplina;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();
   
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $gradeDisciplina->removerDisciplinaGradeById($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }
  
  public function getDisciplinaGradeById(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplina = $this->gradeDisciplina;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();


    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $gradeDisciplina->getDisciplinaGradeById($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }

  public function salvarGradeDisciplinaController(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplina = $this->gradeDisciplina;
    $parsedBody = $request->getParsedBody();
    $dados = $parsedBody["gradeDisciplina"];
    $return = array();

    
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");

    $result = $gradeDisciplina->salvarGradeDisciplina($dados);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }

}



?>