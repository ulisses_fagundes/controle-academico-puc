<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/grade/class/model/GradeDisciplinaRequisito.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class GradeDisciplinaRequisitoController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;
  private $gradeDisciplinaRequisito;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();
     //  $this->usuario = new \App\Models\Usuario();
      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->gradeDisciplinaRequisito = new \App\Models\GradeDisciplinaRequisito();
      
  }

  public function removerRequisitoController(Request $request, Response $response, array $args)
  {
 
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplinaRequisito = $this->gradeDisciplinaRequisito;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();
   
    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $gradeDisciplinaRequisito->removerRequisito($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }
  
  public function getRequisitoFormController(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplinaRequisito = $this->gradeDisciplinaRequisito;
    $parsedBody = $request->getParsedBody();
    $id = $parsedBody["id"];
    $return = array();


    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");
    $result = $gradeDisciplinaRequisito->getGradeDisciplinaRequisitoById($id);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }    
  }

 

  public function salvarRequisitoController(Request $request, Response $response, array $args)
  {
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplinaRequisito = $this->gradeDisciplinaRequisito;
    $parsedBody = $request->getParsedBody();
    $dados = $parsedBody["requisito"];
    $return = array();

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, "grade");

    $result = $gradeDisciplinaRequisito->salvarRequisito($dados);
    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }


  public function getRequisitoGridController(Request $request, Response $response, array $args)
  {
    $usuario = $this->usuario;
    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $gradeDisciplinaRequisito = $this->gradeDisciplinaRequisito;
    $parsedBody = $request->getParsedBody();
    $filtro = isset($parsedBody["filtro"])  ? $parsedBody["filtro"] : null;
    $return = array();    

    if(!isset($filtro["gradeDisciplina"]) || $filtro["gradeDisciplina"] == "" )
    {
      return false;
    }

    Menu::verificarPermissaoTela($this->pdo,/*3*/ $usuario, $request, $response, "grade");
    $result = $gradeDisciplinaRequisito->getRequisitoGrid($pdo, $params, $filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }  
  }    

}



?>