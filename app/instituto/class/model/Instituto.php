<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Instituto extends Model
{
   protected $table = 'instituto';
   protected $fillable = ["instituto", "nome"];
   public $timestamps  = false;
   protected $primaryKey = "instituto";



}

?>