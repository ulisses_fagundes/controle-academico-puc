<?php
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
   protected $table = 'pessoa';
   protected $fillable = 
   [    
        "pessoa", 
        "nome", 
        "cpf", 
        "endereco", 
        "telefone", 
        "celular", 
        "sexo", 
        "documento_identidade", 
        "documento_militar", 
        "documento_eleitoral", 
        "documento_passaporte"
   ];

   public $timestamps  = false;
   protected $primaryKey = "pessoa";

   public function getPessoaAutoComplete($params)
   {
  
       return array
       (
           'list' =>
            $this
                ->select("pessoa", "nome as label")
                ->where('nome', 'ilike','%'.  ($params["termo"]) . '%')
                ->limit($params["limite"])
                ->get()
            
        );

   }   

}

?>