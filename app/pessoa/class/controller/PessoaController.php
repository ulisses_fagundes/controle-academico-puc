<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once $_SERVER["DOCUMENT_ROOT"].'/academico/config/EloquentConfig.php';
require_once $_ENV["BASE_DIR"]."/app/pessoa/class/model/Pessoa.php";

require_once $_ENV["BASE_DIR"]."/helper/Menu.php";

use EloquentConfig\EloquentConfig as EloquentConfig;

class PessoaController
{
  private $twig;
  private $pdo;
  private $eloquent;
  private $usuario;
  private $menu;
  private $params;

  public function __construct($container)
  {

      $this->twig = $container["twig"];
      $this->eloquent =  (\EloquentConfig\EloquentConfig::getCapsule());
      $this->pdo = \Conexao\Conexao::getInstance();

      $this->usuario = $container["usuario"];
      $this->menu = $container["menu"];
      $this->params = $container["params"];
      $this->pessoa = new \App\Models\Pessoa();
      
  }

  public  function getPessoaAutoComplete(Request $request, Response $response, array $args)
  {

    $twig = $this->twig;
    $params = $this->params;
    $pdo = $this->pdo;
    $pessoa = $this->pessoa;
    $parsedBody = $request->getParsedBody();
    $filtro = $parsedBody["filtro"];
    $return = array(); 
    $permissoesPermitidas = array
    (
      "cursos" => "cursos"
    );   
    $permissao = null;
    if(isset($parsedBody["origem"]) && in_array($parsedBody["origem"], $permissoesPermitidas))
    {
      $permissao = $permissoesPermitidas[$parsedBody["origem"]];
    }

    Menu::verificarPermissaoTela($this->pdo,$this->usuario, $request, $response, $permissao);
    $result = $pessoa->getPessoaAutoComplete($filtro);

    if(count($result)> 0)
    {
        return $response->withJson($result);
    }
    else
    {
      // errorPage
    }
  }  



}



?>